#!/usr/bin/env python
# -*- coding: utf-8 -*-

import paho.mqtt.client as mqtt
import time
import random


####################################################################################################
# Definiton of functions
####################################################################################################

def on_connect(client, userdata, flags, rc):
    print("Connected with result code " + str(rc))
    #client.subscribe("/dev1/trickler/esp/scale_request_tare", 0)


####################################################################################################
# Definiton of mqtt client stuff
####################################################################################################


broker_ip="0.0.0.0" #local host of raspberry
#broker_ip="broker.hivemq.com" #local host of raspberry
port=1883

client= mqtt.Client()                          #create client object with random id

client.on_connect = on_connect

client.connect(broker_ip,port) 
client.loop_start()

starttime=time.time()
Counter = 1
send_period = 1


####################################################################################################
# Handle loop
####################################################################################################

while True :
        
    #send each second alive 
    if (time.time() - starttime) > send_period:
        
        fillingtime=random.randint(4100,4200)
        weight=random.randint(2541,2542)

        msg= str(Counter) + ";" + str(fillingtime) + ";" + str(weight)

        client.publish(topic="/dev1/trickler/esp/realtimeplotter",  payload=msg,qos=1, retain=False) 
        print(msg)
        Counter = Counter + 1
        starttime=time.time()   





