#!/usr/bin/env python
# -*- coding: utf-8 -*-

import matplotlib.pyplot as plt
import matplotlib.animation as animation
from matplotlib import style

import paho.mqtt.client as mqtt

####################################################################################################
# global stuff
####################################################################################################

filename = "test.txt"

fig = plt.figure()
plt.title('Open Trickler - Plotter')


ax1 = fig.add_subplot(211)
ax2 = fig.add_subplot(212)


xs = [0]
ys_t = [0]
ys_m = [0]

MaxDataDisplay=30


####################################################################################################
# Definiton of mqtt client stuff
####################################################################################################

def Average(lst): 

    avg = 0
    i = 0
    for val in lst:
        avg = avg + val
        i = i+1
    
    if i > 0:
        avg = avg / i
    else:
        avg = 0

    return avg 

def on_connect(client, userdata, flags, rc):
    print("Connected with result code " + str(rc))
    client.subscribe('/dev1/trickler/esp/realtimeplotter', 0)

def on_message_realtimeplotter(client, userdata, msg):  # The callback for when a PUBLISH message
    #write message in textfile
    file = open(filename,'a')
    file.write(msg.payload)
    file.write('\n')
    file.close()
    
    #split message   
    x, yt, ym = msg.payload.split(';')

    xs.append(int(x))
    ys_t.append(int(yt))
    ys_m.append(int(ym))    

broker_ip="0.0.0.0" #local host of raspberry
port=1883

client= mqtt.Client()                          #create client object with random id

client.message_callback_add("/dev1/trickler/esp/realtimeplotter", on_message_realtimeplotter)
client.on_connect = on_connect


client.connect(broker_ip,port) 
client.loop_start()

####################################################################################################
# Realtime graph
####################################################################################################

#style.use('fivethirtyeight')

def animate(i,xs,ys_t,ys_m):    
    
    xs=xs[-1*MaxDataDisplay:]
    ys_t=ys_t[-1*MaxDataDisplay:]
    ys_m=ys_m[-1*MaxDataDisplay:]
    
    ax1.clear()  
    #ax1.set_xticks(xs, minor=False)
    #ax1.set_yticks(ys_t, minor=False)
    ax1.plot(xs,ys_t, "C1")

    ax1.set_ylabel("Filling Time [ms]")
    ax1.set_xlabel("Average Filling Time: " + str(int(Average(ys_t))) + " ms - " + "Average Filling Weigh: " + str(int(Average(ys_m))) + " mg")
    
    ax2.clear()
    #ax2.set_xticks(xs, minor=False)
    #ax2.set_yticks(ys_m, minor=False)   
    ax2.plot(xs,ys_m, "C2") 

    ax2.set_ylabel("Filling Weight [mg]")

ani = animation.FuncAnimation(fig, animate, fargs=(xs, ys_t,ys_m), interval=1000)
plt.show()
