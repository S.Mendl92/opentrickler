#!/usr/bin/python3

from tkinter import filedialog
from tkinter import messagebox
from tkinter import *               
import tkinter.ttk
from datetime import datetime       
from time import sleep
import paho.mqtt.client as mqtt 
import os
import subprocess

###############################################################################################
#required components
###############################################################################################

#sudo apt-get install python3-tk
#pip3 install paho-mqtt
#pip3 install esptool
#pip3 install DateTime
#pip3 install pyserial

#set active user (sim for simulation, pi as regular user)
user="pi"
#user="sim"

class OpenTricklerUi:
    def __init__(self, master):
        
        ########################################################################################
        #places mqtt stuff
        ########################################################################################

        _broker_ip="0.0.0.0" #local host of raspberry
        _port=1883
        
        self.client= mqtt.Client()
        self.client.on_connect = self.on_connect
        self.client.on_message = self.on_message

        self.client.connect(_broker_ip,_port) 
        self.client.loop_start()

        self._qos = 0

        ########################################################################################
        #places filepathes
        ########################################################################################
        
        if user=="pi":
            self._path_recipes = "../../../Recipes/"
        else:
            self._path_recipes = "Recipes/"

        self._path_openscale = "tools/OpenScaleWraper/"
        self._path_log = "log/"

        ########################################################################################
        #init user interface
        ########################################################################################

        self.master = master
        master.title("OpenTrickler UserInterface")
        
        #defines padsize (space between elements)
        _padsize_x = 2
        _padsize_y = 1
        
        _txt_height = 1
        _txt_width = 10

        ########################################################################################
        #places sperator control / recipe
        ########################################################################################

        tkinter.ttk.Separator(master, orient=VERTICAL).grid(column=8, row=0, rowspan=40, sticky='ns')
        tkinter.ttk.Separator(master, orient=VERTICAL).grid(column=9, row=0, rowspan=40, sticky='ns')

        tkinter.ttk.Separator(master, orient=VERTICAL).grid(column=15, row=0, rowspan=40, sticky='ns')
        tkinter.ttk.Separator(master, orient=VERTICAL).grid(column=16, row=0, rowspan=40, sticky='ns')

        ########################################################################################
        #places control menue
        ########################################################################################

        _cm_start_row = 0
        _cm_start_col = 0
        _cm_act_row = 0

        self.lbl_control_menue_display = Label(master,bg="white",justify="left",anchor="w", width=_txt_width * 3, text="Display:  \n ---")
        self.lbl_control_menue_display.grid(row=_cm_start_row + _cm_act_row, column= _cm_start_col+0, columnspan = 3,  padx=_padsize_x, pady=_padsize_y, sticky="w")
        
        self.lbl_control_menue_open_scale_connection = Label(master,bg="red", width=_txt_width, text="Open\nScale")
        self.lbl_control_menue_open_scale_connection.grid(row=_cm_start_row + _cm_act_row, column= _cm_start_col + 4,  padx=_padsize_x, pady=_padsize_y)
        
        _cm_act_row += 1

        _oplst_control_menue_opmode = ["Automatic Action Mode",
                                        "Single Action Mode",
                                        "Setting Mode - FineFiller",
                                        "Setting Mode - PreFiller"]
        
        self._var_oplst_control_menue_opmode = StringVar(master)
        self._var_oplst_control_menue_opmode.set(_oplst_control_menue_opmode[0])

        self._var_oplst_control_menue_opmode.trace("w",self.qf_oplst_control_menue_opmode)
        
        self.oplst_control_menue_opmode = OptionMenu(master,  self._var_oplst_control_menue_opmode,*_oplst_control_menue_opmode)
        self.oplst_control_menue_opmode.config(height=_txt_height, width=_txt_width*4-4)
        self.oplst_control_menue_opmode.grid(row=_cm_start_row + _cm_act_row, column= _cm_start_col + 0,columnspan = 5,  padx=_padsize_x, pady=_padsize_y)
                
        _cm_act_row += 1

        _oplst_control_menue_setting_finefiller = ["disabled",
                                                    "1/1 Stepmode",
                                                    "1/2 Stepmode",
                                                    "1/4 Stepmode",
                                                    "1/8 Stepmode",
                                                    "1/16 Stepmode"]
        
        self._var_oplst_control_menue_setting_finefiller = StringVar(master)
        self._var_oplst_control_menue_setting_finefiller.set(_oplst_control_menue_setting_finefiller[0])

        self._var_oplst_control_menue_setting_finefiller.trace("w",self.qf_oplst_control_menue_setting_finefiller)
        
        self.oplst_control_menue_setting_finefiller = OptionMenu(master,  self._var_oplst_control_menue_setting_finefiller,*_oplst_control_menue_setting_finefiller)
        self.oplst_control_menue_setting_finefiller.config(height=_txt_height, width=_txt_width*4-4)
        self.oplst_control_menue_setting_finefiller.grid(row=_cm_start_row + _cm_act_row, column= _cm_start_col + 0,columnspan = 5,  padx=_padsize_x, pady=_padsize_y)
        
        _cm_act_row += 1

        self.btn_control_menue_single_action = Button(master,width=_txt_width,  text="single action", command=self.qf_control_menue_single_action)
        self.btn_control_menue_single_action.grid(row=_cm_start_row + _cm_act_row, column= _cm_start_col + 0,  padx=_padsize_x, pady=_padsize_y)

        self.btn_control_menue_abort = Button(master,width=_txt_width,  text="abort", command=self.qf_control_menue_abort)
        self.btn_control_menue_abort.grid(row=_cm_start_row + _cm_act_row, column= _cm_start_col + 2,  padx=_padsize_x, pady=_padsize_y)
        
        #https://codeloop.org/how-to-create-progressbar-in-python-tkinter/
        self.prgb_control_menue_progress= tkinter.ttk.Progressbar(master,orient = 'horizontal', length = 80, mode = 'determinate')
        self.prgb_control_menue_progress.grid(row=_cm_start_row + _cm_act_row, column= _cm_start_col + 4, columnspan = 1, padx=_padsize_x, pady=_padsize_y)
        
        _cm_act_row += 1

        #draw horizontal boarder
        tkinter.ttk.Separator(master, orient=HORIZONTAL).grid(row=_cm_start_row + _cm_act_row ,column=_cm_start_col, columnspan=5, sticky='we')
            
        ########################################################################################
        #places scale menue
        ########################################################################################

        _sm_start_row = _cm_start_row + _cm_act_row + 1
        _sm_start_col = 0
        _sm_act_row = 0

        self.btn_scale_menue_tare = Button(master,width=_txt_width,  text="tare", command=self.qf_scale_menue_tare)
        self.btn_scale_menue_tare.grid(row=_sm_start_row + _sm_act_row, column= _sm_start_col + 0,  padx=_padsize_x, pady=_padsize_y)

        self.lbl_scale_menue_weight = Label(master,bg="white",anchor="w", width=int(_txt_width*2.6),text="weight: ---")
        self.lbl_scale_menue_weight.grid(row=_sm_start_row + _sm_act_row, column= _sm_start_col + 2, columnspan = 3, padx=_padsize_x, pady=_padsize_y)
        
        _sm_act_row += 1

        self.btn_scale_menue_connect = Button(master,width=_txt_width,  text="connect", command=self.qf_scale_menue_connect)
        self.btn_scale_menue_connect.grid(row=_sm_start_row + _sm_act_row, column= _sm_start_col + 0,  padx=_padsize_x, pady=_padsize_y)

        _sm_act_row += 1

        #draw horizontal boarder
        tkinter.ttk.Separator(master, orient=HORIZONTAL).grid(row=_sm_start_row + _sm_act_row ,column=_sm_start_col, columnspan=5, sticky='we')
        
        ########################################################################################
        #places debug menue
        ########################################################################################

        _dm_start_row = _sm_start_row + _sm_act_row + 1
        _dm_start_col = 0
        _dm_act_row = 0     

        self.lbl_debug_menue_esp_alive = Label(master,bg="white",anchor="w", width=int(_txt_width*4.1),text="ESP Alive: ---")
        self.lbl_debug_menue_esp_alive.grid(row=_dm_start_row + _dm_act_row, column= _dm_start_col + 0, columnspan = 5, padx=_padsize_x, pady=_padsize_y)
        
        _dm_act_row += 1

        self.lbl_debug_menue_openscale_alive = Label(master,bg="white",anchor="w", width=int(_txt_width*4.1),text="Open Scale Alive: ---")
        self.lbl_debug_menue_openscale_alive.grid(row=_dm_start_row + _dm_act_row, column= _dm_start_col + 0, columnspan = 5, padx=_padsize_x, pady=_padsize_y)

        _dm_act_row += 1

        #draw horizontal boarder
        tkinter.ttk.Separator(master, orient=HORIZONTAL).grid(row=_dm_start_row + _dm_act_row ,column=_dm_start_col, columnspan=5, sticky='we')
        
        ########################################################################################
        #places flash menue
        ########################################################################################

        _fm_start_row = _dm_start_row + _dm_act_row + 1
        _fm_start_col = 0
        _fm_act_row = 0

        self.lbl_flash_menue_header = Label(master, text="flash menue:")
        self.lbl_flash_menue_header.grid(row=_fm_start_row + _fm_act_row, column= _fm_start_col + 0, columnspan=5 ,  padx=_padsize_x, pady=_padsize_y, sticky="w")
        
        _fm_act_row += 1

        self.lbl_flash_menue_port = Label(master, text="port:")
        self.lbl_flash_menue_port.grid(row=_fm_start_row + _fm_act_row, column= _fm_start_col + 0, columnspan=1 ,  padx=_padsize_x, pady=_padsize_y, sticky="we")
                
        _oplst_flash_menue_ports = ["/dev/ttyUSB0",
                                    "/dev/ttyUSB1",
                                    "/dev/ttyUSB2",
                                    "/dev/ttyUSB3"]
        
        self._var_oplst_flash_menue_ports = StringVar(master)
        self._var_oplst_flash_menue_ports.set(_oplst_flash_menue_ports[1])
        
        self.oplst_flash_menue_ports = OptionMenu(master,  self._var_oplst_flash_menue_ports,*_oplst_flash_menue_ports)
        self.oplst_flash_menue_ports.config(height=_txt_height, width=_txt_width*3-8)
        self.oplst_flash_menue_ports.grid(row=_fm_start_row + _fm_act_row, column= _fm_start_col + 1,columnspan = 4,  padx=_padsize_x, pady=_padsize_y)
                
        _fm_act_row += 1

        self.lbl_flash_menue_baud = Label(master, text="baud:")
        self.lbl_flash_menue_baud.grid(row=_fm_start_row + _fm_act_row, column= _fm_start_col + 0, columnspan=1 ,  padx=_padsize_x, pady=_padsize_y, sticky="we")
        
        _oplst_flash_menue_baud = ["300",
                                   "600",
                                   "1200",
                                   "2400",
                                   "4800",
                                   "9600",
                                   "19200",
                                   "38400",
                                   "57600",
                                   "115200",
                                   "230400"]
        
        self._var_oplst_flash_menue_baud = StringVar(master)
        self._var_oplst_flash_menue_baud.set(_oplst_flash_menue_baud[9])
        
        self.oplst_flash_menue_baud = OptionMenu(master,  self._var_oplst_flash_menue_baud,*_oplst_flash_menue_baud)
        self.oplst_flash_menue_baud.config(height=_txt_height, width=_txt_width*3-8)
        self.oplst_flash_menue_baud.grid(row=_fm_start_row + _fm_act_row, column= _fm_start_col + 1,columnspan = 4,  padx=_padsize_x, pady=_padsize_y)
        
        _fm_act_row += 1

        self.btn_flash_menue_load_file = Button(master, width=_txt_width, text="load file", command=self.qf_flash_menue_load_file)
        self.btn_flash_menue_load_file.grid(row=_fm_start_row + _fm_act_row, column= _fm_start_col + 0,  padx=_padsize_x, pady=_padsize_y)
        
        self.lbl_flash_menue_filepath = Label(master,bg="white",width=_txt_width*3-4, text="---")
        self.lbl_flash_menue_filepath.grid(row=_fm_start_row + _fm_act_row, column= _fm_start_col + 1,columnspan = 4,  padx=_padsize_x, pady=_padsize_y)
        
        _fm_act_row += 1

        self.btn_flash_menue_flash = Button(master, width=_txt_width*4-2, text="flash esp", command=self.qf_flash_menue_flash_esp)
        self.btn_flash_menue_flash.grid(row=_fm_start_row + _fm_act_row, column= _fm_start_col + 0,columnspan = 5,   padx=_padsize_x, pady=_padsize_y)
        
        ########################################################################################
        #places recipe save menue
        ########################################################################################
                
        _rsm_start_row = 0
        _rsm_start_col = 10
        _rsm_act_row = 0
        
        self.lbl_header1_select_recipe = Label(master, text="select recipe")
        self.lbl_header1_select_recipe.grid(row=_rsm_start_row + _rsm_act_row, column= _rsm_start_col, columnspan=1 ,  padx=_padsize_x, pady=_padsize_y, sticky="we")
        
        #add recipe folger if missing
        if not os.path.isdir(self._path_recipes):
            os.mkdir(self._path_recipes)

            #add default recipe if no recipe is available
            if not os.listdir(self._path_recipes):
                f = open(self._path_recipes + "Default", "a")
                f.close

        _oplst_recipe_save_menue = os.listdir(self._path_recipes) 
        
        self._var_oplst_recipe_save_menue = StringVar(master)
        self._var_oplst_recipe_save_menue.set(_oplst_recipe_save_menue[0])
        
        self.oplst_recipe_save_menue= OptionMenu(master, self._var_oplst_recipe_save_menue,*_oplst_recipe_save_menue)
        self.oplst_recipe_save_menue.config(height=_txt_height, width=int(_txt_width*3.7))
        self.oplst_recipe_save_menue.grid(row=_rsm_start_row + _rsm_act_row, column= _rsm_start_col + 1, columnspan= 3,  padx=_padsize_x, pady=_padsize_y)

        self.lbl_recipe_save_menue_upload_id = Label(master,bg="white", width=_txt_width, text="--- ")
        self.lbl_recipe_save_menue_upload_id.grid(row=_rsm_start_row + _rsm_act_row, column= _rsm_start_col+4, padx=_padsize_x, pady=_padsize_y, sticky="we")
        
        
        _rsm_act_row += 1
        
        self.btn_recipe_save_menue_load = Button(master, width=_txt_width, text="load", command=self.qf_save_menue_load)
        self.btn_recipe_save_menue_load.grid(row=_rsm_start_row + _rsm_act_row, column= _rsm_start_col + 0,  padx=_padsize_x, pady=_padsize_y)
        
        self.btn_recipe_save_menue_save = Button(master,width=_txt_width,  text="save", command=self.qf_save_menue_save)
        self.btn_recipe_save_menue_save.grid(row=_rsm_start_row + _rsm_act_row, column= _rsm_start_col + 1,  padx=_padsize_x, pady=_padsize_y)
        
        self.btn_recipe_save_menue_set_default = Button(master,width=_txt_width,  text="set default", command=self.qf_save_menue_default)
        self.btn_recipe_save_menue_set_default.grid(row=_rsm_start_row + _rsm_act_row, column= _rsm_start_col + 2,  padx=_padsize_x, pady=_padsize_y)
        
        self.btn_recipe_save_menue_create = Button(master,width=_txt_width,  text="create", command=self.qf_save_menue_create)
        self.btn_recipe_save_menue_create.grid(row=_rsm_start_row + _rsm_act_row, column= _rsm_start_col + 3,  padx=_padsize_x, pady=_padsize_y)

        self.btn_recipe_save_download_all = Button(master,width=_txt_width,  text="download all", command=self.qf_save_menue_download_all)
        self.btn_recipe_save_download_all.grid(row=_rsm_start_row + _rsm_act_row, column= _rsm_start_col + 4,  padx=_padsize_x, pady=_padsize_y)    
        
        ########################################################################################
        #places recipe parameter menue
        ########################################################################################
        
        _rpm_start_row = _rsm_start_row + _rsm_act_row + 1
        _rpm_start_col = _rsm_start_col
        _rpm_act_row = 0        
             
        #parameter menue -> filling parameter
        
        _rpm_act_row += 0
        
        #draw horizontal boarder
        tkinter.ttk.Separator(master, orient=HORIZONTAL).grid(row=_rpm_start_row + _rpm_act_row,column=_rpm_start_col, columnspan=5, sticky='we')
                
        #parameter Menue -> filling weight
        
        _rpm_act_row += 1
        
        self.lbl_recipe_value_filling_weight = Label(master,text="filling weight")
        self.lbl_recipe_value_filling_weight.grid(row=_rpm_start_row + _rpm_act_row, column=_rpm_start_col + 0, padx=_padsize_x, pady=_padsize_y, sticky="w")

        self.lbl_recipe_value_filling_weight_unit = Label(master, text="[mg]")
        self.lbl_recipe_value_filling_weight_unit.grid(row=_rpm_start_row + _rpm_act_row, column= _rpm_start_col + 1,  padx=_padsize_x, pady=_padsize_y)
        
        self.txt_recipe_value_filling_weight_hmi = Text(master, height=_txt_height, width=_txt_width)
        self.txt_recipe_value_filling_weight_hmi.grid(row=_rpm_start_row + _rpm_act_row, column= _rpm_start_col + 2,  padx=_padsize_x, pady=_padsize_y)
        
        self.lbl_recipe_value_filling_weight_esp = Label(master,bg="white", width=_txt_width, text="---")
        self.lbl_recipe_value_filling_weight_esp.grid(row=_rpm_start_row + _rpm_act_row, column= _rpm_start_col + 3,  padx=_padsize_x, pady=_padsize_y)
        
        self.btn_recipe_value_filling_weight = Button(master,width=_txt_width,  text="download", command=self.qf_recipe_value_filling_weight)
        self.btn_recipe_value_filling_weight.grid(row=_rpm_start_row + _rpm_act_row, column= _rpm_start_col + 4,  padx=_padsize_x, pady=_padsize_y)
        
        #parameter menue -> prefiller header
        
        _rpm_act_row += 1
        
        #draw horizontal boarder
        tkinter.ttk.Separator(master, orient=HORIZONTAL).grid(row=_rpm_start_row + _rpm_act_row,column=_rpm_start_col, columnspan=5, sticky='we')
        
        _rpm_act_row += 1
        
        self.lbl_header1_prefiller = Label(master, text="prefiller parameter menue:")
        self.lbl_header1_prefiller.grid(row=_rpm_start_row + _rpm_act_row, column= _rpm_start_col, columnspan=4 ,  padx=_padsize_x, pady=_padsize_y, sticky="w")
                
        #parameter menue -> prefiller -> velocity fast
        
        _rpm_act_row += 1
        
        self.lbl_recipe_value_prefiller_velocity_fast = Label(master, text="velocity fast")
        self.lbl_recipe_value_prefiller_velocity_fast.grid(row=_rpm_start_row + _rpm_act_row, column=_rpm_start_col + 0, padx=_padsize_x, pady=_padsize_y, sticky="w")

        self.lbl_recipe_value_prefiller_velocity_fast_unit = Label(master, text="[steps/s]")
        self.lbl_recipe_value_prefiller_velocity_fast_unit.grid(row=_rpm_start_row + _rpm_act_row, column= _rpm_start_col + 1,  padx=_padsize_x, pady=_padsize_y)
        
        self.txt_recipe_value_prefiller_velocity_fast_hmi = Text(master, height=_txt_height, width=_txt_width)
        self.txt_recipe_value_prefiller_velocity_fast_hmi.grid(row=_rpm_start_row + _rpm_act_row, column= _rpm_start_col + 2,  padx=_padsize_x, pady=_padsize_y)
        
        self.lbl_recipe_value_prefiller_velocity_fast_esp = Label(master,bg="white", width=_txt_width, text="---")
        self.lbl_recipe_value_prefiller_velocity_fast_esp.grid(row=_rpm_start_row + _rpm_act_row, column= _rpm_start_col + 3,  padx=_padsize_x, pady=_padsize_y)
        
        self.btn_recipe_value_prefiller_velocity_fast = Button(master,width=_txt_width,  text="download", command=self.qf_recipe_value_prefiller_velocity_fast)
        self.btn_recipe_value_prefiller_velocity_fast.grid(row=_rpm_start_row + _rpm_act_row, column= _rpm_start_col + 4,  padx=_padsize_x, pady=_padsize_y)
        
        #parameter menue -> prefiller -> weight fast
        
        _rpm_act_row += 1
        
        self.lbl_recipe_value_prefiller_weight_slow = Label(master, text="weight slow")
        self.lbl_recipe_value_prefiller_weight_slow.grid(row=_rpm_start_row + _rpm_act_row, column=_rpm_start_col + 0, padx=_padsize_x, pady=_padsize_y, sticky="w")

        self.lbl_recipe_value_prefiller_weight_slow_unit = Label(master, text="[mg]")
        self.lbl_recipe_value_prefiller_weight_slow_unit.grid(row=_rpm_start_row + _rpm_act_row, column= _rpm_start_col + 1,  padx=_padsize_x, pady=_padsize_y)
        
        self.txt_recipe_value_prefiller_weight_slow_hmi = Text(master, height=_txt_height, width=_txt_width)
        self.txt_recipe_value_prefiller_weight_slow_hmi.grid(row=_rpm_start_row + _rpm_act_row, column= _rpm_start_col + 2,  padx=_padsize_x, pady=_padsize_y)
        
        self.lbl_recipe_value_prefiller_weight_slow_esp = Label(master,bg="white", width=_txt_width, text="---")
        self.lbl_recipe_value_prefiller_weight_slow_esp.grid(row=_rpm_start_row + _rpm_act_row, column= _rpm_start_col + 3,  padx=_padsize_x, pady=_padsize_y)
        
        self.btn_recipe_value_prefiller_weight_slow = Button(master,width=_txt_width,  text="download", command=self.qf_recipe_value_prefiller_weight_slow)
        self.btn_recipe_value_prefiller_weight_slow.grid(row=_rpm_start_row + _rpm_act_row, column= _rpm_start_col + 4,  padx=_padsize_x, pady=_padsize_y)
        
        #parameter menue -> prefiller -> velocity slow
        
        _rpm_act_row += 1
        
        self.lbl_recipe_value_prefiller_velocity_slow = Label(master, text="velocity slow")
        self.lbl_recipe_value_prefiller_velocity_slow.grid(row=_rpm_start_row + _rpm_act_row, column=_rpm_start_col + 0, padx=_padsize_x, pady=_padsize_y, sticky="w")

        self.lbl_recipe_value_prefiller_velocity_slow_unit = Label(master, text="[steps/s]")
        self.lbl_recipe_value_prefiller_velocity_slow_unit.grid(row=_rpm_start_row + _rpm_act_row, column= _rpm_start_col + 1,  padx=_padsize_x, pady=_padsize_y)
        
        self.txt_recipe_value_prefiller_velocity_slow_hmi = Text(master, height=_txt_height, width=_txt_width)
        self.txt_recipe_value_prefiller_velocity_slow_hmi.grid(row=_rpm_start_row + _rpm_act_row, column= _rpm_start_col + 2,  padx=_padsize_x, pady=_padsize_y)
        
        self.lbl_recipe_value_prefiller_velocity_slow_esp = Label(master,bg="white", width=_txt_width, text="---")
        self.lbl_recipe_value_prefiller_velocity_slow_esp.grid(row=_rpm_start_row + _rpm_act_row, column= _rpm_start_col + 3,  padx=_padsize_x, pady=_padsize_y)
        
        self.btn_recipe_value_prefiller_velocity_slow = Button(master,width=_txt_width,  text="download", command=self.qf_recipe_value_prefiller_velocity_slow)
        self.btn_recipe_value_prefiller_velocity_slow.grid(row=_rpm_start_row + _rpm_act_row, column= _rpm_start_col + 4,  padx=_padsize_x, pady=_padsize_y)
                        
        #parameter menue -> prefiller -> weight slow
        
        _rpm_act_row += 1
        
        self.lbl_recipe_value_prefiller_weight_stop = Label(master, text="weight stop")
        self.lbl_recipe_value_prefiller_weight_stop.grid(row=_rpm_start_row + _rpm_act_row, column=_rpm_start_col + 0, padx=_padsize_x, pady=_padsize_y, sticky="w")

        self.lbl_recipe_value_prefiller_weight_stop_unit = Label(master, text="[mg]")
        self.lbl_recipe_value_prefiller_weight_stop_unit.grid(row=_rpm_start_row + _rpm_act_row, column= _rpm_start_col + 1,  padx=_padsize_x, pady=_padsize_y)
        
        self.txt_recipe_value_prefiller_weight_stop_hmi = Text(master, height=_txt_height, width=_txt_width)
        self.txt_recipe_value_prefiller_weight_stop_hmi.grid(row=_rpm_start_row + _rpm_act_row, column= _rpm_start_col + 2,  padx=_padsize_x, pady=_padsize_y)
        
        self.lbl_recipe_value_prefiller_weight_stop_esp = Label(master,bg="white", width=_txt_width, text="---")
        self.lbl_recipe_value_prefiller_weight_stop_esp.grid(row=_rpm_start_row + _rpm_act_row, column= _rpm_start_col + 3,  padx=_padsize_x, pady=_padsize_y)
        
        self.btn_recipe_value_prefiller_weight_stop = Button(master,width=_txt_width,  text="download", command=self.qf_recipe_value_prefiller_weight_stop)
        self.btn_recipe_value_prefiller_weight_stop.grid(row=_rpm_start_row + _rpm_act_row, column= _rpm_start_col + 4,  padx=_padsize_x, pady=_padsize_y)
        

    
        # parameter menue -> prefiller -> handle finefiller
        
        _rpm_act_row += 1
        
        self.lbl_recipe_value_prefiller_handle_finefiller = Label(master, text="handle finefiller")
        self.lbl_recipe_value_prefiller_handle_finefiller.grid(row=_rpm_start_row + _rpm_act_row, column=_rpm_start_col + 0, padx=_padsize_x, pady=_padsize_y, sticky="w")

        self.lbl_recipe_value_prefiller_handle_finefiller_unit = Label(master, text="---")
        self.lbl_recipe_value_prefiller_handle_finefiller_unit.grid(row=_rpm_start_row + _rpm_act_row, column= _rpm_start_col + 1,  padx=_padsize_x, pady=_padsize_y)
                
        #https://www.delftstack.com/de/howto/python-tkinter/how-to-create-dropdown-menu-in-tkinter/
        #https://effbot.org/tkinterbook/optionmenu.htm
        _oplst_recipe_value_prefiller_handle_finefiller = ["disable", "enable"]
        
        self._var_oplst_recipe_value_prefiller_handle_finefiller = StringVar(master)
        self._var_oplst_recipe_value_prefiller_handle_finefiller.set(_oplst_recipe_value_prefiller_handle_finefiller[0])
        
        self.oplst_recipe_value_prefiller_handle_finefiller_hmi = OptionMenu(master,  self._var_oplst_recipe_value_prefiller_handle_finefiller,*_oplst_recipe_value_prefiller_handle_finefiller)
        self.oplst_recipe_value_prefiller_handle_finefiller_hmi.config(height=_txt_height, width=_txt_width-4)
        self.oplst_recipe_value_prefiller_handle_finefiller_hmi.grid(row=_rpm_start_row + _rpm_act_row, column= _rpm_start_col + 2,  padx=_padsize_x, pady=_padsize_y)
                
        self.lbl_recipe_value_prefiller_handle_finefiller_esp = Label(master,bg="white", width=_txt_width, text="---")
        self.lbl_recipe_value_prefiller_handle_finefiller_esp.grid(row=_rpm_start_row + _rpm_act_row, column= _rpm_start_col + 3,  padx=_padsize_x, pady=_padsize_y)
        
        self.btn_recipe_value_prefiller_handle_finefiller = Button(master,width=_txt_width,  text="download", command=self.qf_recipe_value_prefiller_handle_finefiller)
        self.btn_recipe_value_prefiller_handle_finefiller.grid(row=_rpm_start_row + _rpm_act_row, column= _rpm_start_col + 4,  padx=_padsize_x, pady=_padsize_y)
        
        #parameter menue -> finefiller header
        
        _rpm_act_row += 1
        
        tkinter.ttk.Separator(master, orient=HORIZONTAL).grid(row=_rpm_start_row + _rpm_act_row,column=_rpm_start_col, columnspan=5, sticky='we')
        
        _rpm_act_row += 1
        
        self.lbl_header2_finefiller = Label(master, text="finefiller parameter menue:")
        self.lbl_header2_finefiller.grid(row=_rpm_start_row + _rpm_act_row, column= _rpm_start_col, columnspan=4 ,  padx=_padsize_x, pady=_padsize_y, sticky="w")

        #parameter menue -> finefiller velocity full 1/1

        _rpm_act_row += 1
        
        self.lbl_recipe_value_finefiller_velocity_1_1 = Label(master, text="velocity full - 1/1")
        self.lbl_recipe_value_finefiller_velocity_1_1.grid(row=_rpm_start_row + _rpm_act_row, column=_rpm_start_col + 0, padx=_padsize_x, pady=_padsize_y, sticky="w")

        self.lbl_recipe_value_finefiller_velocity_1_1_unit = Label(master, text="[steps/ms]")
        self.lbl_recipe_value_finefiller_velocity_1_1_unit.grid(row=_rpm_start_row + _rpm_act_row, column= _rpm_start_col + 1,  padx=_padsize_x, pady=_padsize_y)
        
        self.txt_recipe_value_finefiller_velocity_1_1_hmi = Text(master, height=_txt_height, width=_txt_width)
        self.txt_recipe_value_finefiller_velocity_1_1_hmi.grid(row=_rpm_start_row + _rpm_act_row, column= _rpm_start_col + 2,  padx=_padsize_x, pady=_padsize_y)
        
        self.lbl_recipe_value_finefiller_velocity_1_1_esp = Label(master,bg="white", width=_txt_width, text="---")
        self.lbl_recipe_value_finefiller_velocity_1_1_esp.grid(row=_rpm_start_row + _rpm_act_row, column= _rpm_start_col + 3,  padx=_padsize_x, pady=_padsize_y)
        
        self.btn_recipe_value_finefiller_velocity_1_1 = Button(master,width=_txt_width,  text="download", command=self.qf_recipe_value_finefiller_velocity_1_1)
        self.btn_recipe_value_finefiller_velocity_1_1.grid(row=_rpm_start_row + _rpm_act_row, column= _rpm_start_col + 4,  padx=_padsize_x, pady=_padsize_y)
                        
        #parameter menue -> finefiller velocity half 1/2

        _rpm_act_row += 1
        
        self.lbl_recipe_value_finefiller_velocity_1_2 = Label(master, text="velocity half - 1/2")
        self.lbl_recipe_value_finefiller_velocity_1_2.grid(row=_rpm_start_row + _rpm_act_row, column=_rpm_start_col + 0, padx=_padsize_x, pady=_padsize_y, sticky="w")

        self.lbl_recipe_value_finefiller_velocity_1_2_unit = Label(master, text="[steps/ms]")
        self.lbl_recipe_value_finefiller_velocity_1_2_unit.grid(row=_rpm_start_row + _rpm_act_row, column= _rpm_start_col + 1,  padx=_padsize_x, pady=_padsize_y)
        
        self.txt_recipe_value_finefiller_velocity_1_2_hmi = Text(master, height=_txt_height, width=_txt_width)
        self.txt_recipe_value_finefiller_velocity_1_2_hmi.grid(row=_rpm_start_row + _rpm_act_row, column= _rpm_start_col + 2,  padx=_padsize_x, pady=_padsize_y)
        
        self.lbl_recipe_value_finefiller_velocity_1_2_esp = Label(master,bg="white", width=_txt_width, text="---")
        self.lbl_recipe_value_finefiller_velocity_1_2_esp.grid(row=_rpm_start_row + _rpm_act_row, column= _rpm_start_col + 3,  padx=_padsize_x, pady=_padsize_y)
        
        self.btn_recipe_value_finefiller_velocity_1_2 = Button(master,width=_txt_width,  text="download", command=self.qf_recipe_value_finefiller_velocity_1_2)
        self.btn_recipe_value_finefiller_velocity_1_2.grid(row=_rpm_start_row + _rpm_act_row, column= _rpm_start_col + 4,  padx=_padsize_x, pady=_padsize_y)


        #parameter menue -> finefiller velocity quarter 1/4

        _rpm_act_row += 1
        
        self.lbl_recipe_value_finefiller_velocity_1_4 = Label(master, text="velocity quarter - 1/4")
        self.lbl_recipe_value_finefiller_velocity_1_4.grid(row=_rpm_start_row + _rpm_act_row, column=_rpm_start_col + 0, padx=_padsize_x, pady=_padsize_y, sticky="w")

        self.lbl_recipe_value_finefiller_velocity_1_4_unit = Label(master, text="[steps/ms]")
        self.lbl_recipe_value_finefiller_velocity_1_4_unit.grid(row=_rpm_start_row + _rpm_act_row, column= _rpm_start_col + 1,  padx=_padsize_x, pady=_padsize_y)
        
        self.txt_recipe_value_finefiller_velocity_1_4_hmi = Text(master, height=_txt_height, width=_txt_width)
        self.txt_recipe_value_finefiller_velocity_1_4_hmi.grid(row=_rpm_start_row + _rpm_act_row, column= _rpm_start_col + 2,  padx=_padsize_x, pady=_padsize_y)
        
        self.lbl_recipe_value_finefiller_velocity_1_4_esp = Label(master,bg="white", width=_txt_width, text="---")
        self.lbl_recipe_value_finefiller_velocity_1_4_esp.grid(row=_rpm_start_row + _rpm_act_row, column= _rpm_start_col + 3,  padx=_padsize_x, pady=_padsize_y)
        
        self.btn_recipe_value_finefiller_velocity_1_4 = Button(master,width=_txt_width,  text="download", command=self.qf_recipe_value_finefiller_velocity_1_4)
        self.btn_recipe_value_finefiller_velocity_1_4.grid(row=_rpm_start_row + _rpm_act_row, column= _rpm_start_col + 4,  padx=_padsize_x, pady=_padsize_y)
                         
        #parameter menue -> finefiller velocity quarter 1/8

        _rpm_act_row += 1
        
        self.lbl_recipe_value_finefiller_velocity_1_8 = Label(master, text="velocity eigth - 1/8")
        self.lbl_recipe_value_finefiller_velocity_1_8.grid(row=_rpm_start_row + _rpm_act_row, column=_rpm_start_col + 0, padx=_padsize_x, pady=_padsize_y, sticky="w")

        self.lbl_recipe_value_finefiller_velocity_1_8_unit = Label(master, text="[steps/ms]")
        self.lbl_recipe_value_finefiller_velocity_1_8_unit.grid(row=_rpm_start_row + _rpm_act_row, column= _rpm_start_col + 1,  padx=_padsize_x, pady=_padsize_y)
        
        self.txt_recipe_value_finefiller_velocity_1_8_hmi = Text(master, height=_txt_height, width=_txt_width)
        self.txt_recipe_value_finefiller_velocity_1_8_hmi.grid(row=_rpm_start_row + _rpm_act_row, column= _rpm_start_col + 2,  padx=_padsize_x, pady=_padsize_y)
        
        self.lbl_recipe_value_finefiller_velocity_1_8_esp = Label(master,bg="white", width=_txt_width, text="---")
        self.lbl_recipe_value_finefiller_velocity_1_8_esp.grid(row=_rpm_start_row + _rpm_act_row, column= _rpm_start_col + 3,  padx=_padsize_x, pady=_padsize_y)
        
        self.btn_recipe_value_finefiller_velocity_1_8 = Button(master,width=_txt_width,  text="download", command=self.qf_recipe_value_finefiller_velocity_1_8)
        self.btn_recipe_value_finefiller_velocity_1_8.grid(row=_rpm_start_row + _rpm_act_row, column= _rpm_start_col + 4,  padx=_padsize_x, pady=_padsize_y)                
        
        #parameter menue -> finefiller velocity sixteenth 1/16

        _rpm_act_row += 1
        
        self.lbl_recipe_value_finefiller_velocity_1_16 = Label(master, text="velocity sixteenth - 1/16")
        self.lbl_recipe_value_finefiller_velocity_1_16.grid(row=_rpm_start_row + _rpm_act_row, column=_rpm_start_col + 0, padx=_padsize_x, pady=_padsize_y, sticky="w")

        self.lbl_recipe_value_finefiller_velocity_1_16_unit = Label(master, text="[steps/ms]")
        self.lbl_recipe_value_finefiller_velocity_1_16_unit.grid(row=_rpm_start_row + _rpm_act_row, column= _rpm_start_col + 1,  padx=_padsize_x, pady=_padsize_y)
        
        self.txt_recipe_value_finefiller_velocity_1_16_hmi = Text(master, height=_txt_height, width=_txt_width)
        self.txt_recipe_value_finefiller_velocity_1_16_hmi.grid(row=_rpm_start_row + _rpm_act_row, column= _rpm_start_col + 2,  padx=_padsize_x, pady=_padsize_y)
        
        self.lbl_recipe_value_finefiller_velocity_1_16_esp = Label(master,bg="white", width=_txt_width, text="---")
        self.lbl_recipe_value_finefiller_velocity_1_16_esp.grid(row=_rpm_start_row + _rpm_act_row, column= _rpm_start_col + 3,  padx=_padsize_x, pady=_padsize_y)
        
        self.btn_recipe_value_finefiller_velocity_1_16 = Button(master,width=_txt_width,  text="download", command=self.qf_recipe_value_finefiller_velocity_1_16)
        self.btn_recipe_value_finefiller_velocity_1_16.grid(row=_rpm_start_row + _rpm_act_row, column= _rpm_start_col + 4,  padx=_padsize_x, pady=_padsize_y)                          

        #parameter menue -> finefiller trickle weight half [1/2]

        _rpm_act_row += 1
        
        self.lbl_recipe_value_finefiller_trickleweight_1_2 = Label(master, text="trickle weight half - 1/2")
        self.lbl_recipe_value_finefiller_trickleweight_1_2.grid(row=_rpm_start_row + _rpm_act_row, column=_rpm_start_col + 0, padx=_padsize_x, pady=_padsize_y, sticky="w")

        self.lbl_recipe_value_finefiller_trickleweight_1_2_unit = Label(master, text="[mg]")
        self.lbl_recipe_value_finefiller_trickleweight_1_2_unit.grid(row=_rpm_start_row + _rpm_act_row, column= _rpm_start_col + 1,  padx=_padsize_x, pady=_padsize_y)
        
        self.txt_recipe_value_finefiller_trickleweight_1_2_hmi = Text(master, height=_txt_height, width=_txt_width)
        self.txt_recipe_value_finefiller_trickleweight_1_2_hmi.grid(row=_rpm_start_row + _rpm_act_row, column= _rpm_start_col + 2,  padx=_padsize_x, pady=_padsize_y)
        
        self.lbl_recipe_value_finefiller_trickleweight_1_2_esp = Label(master,bg="white", width=_txt_width, text="---")
        self.lbl_recipe_value_finefiller_trickleweight_1_2_esp.grid(row=_rpm_start_row + _rpm_act_row, column= _rpm_start_col + 3,  padx=_padsize_x, pady=_padsize_y)
        
        self.btn_recipe_value_finefiller_trickleweight_1_2 = Button(master,width=_txt_width,  text="download", command=self.qf_recipe_value_finefiller_trickleweight_1_2)
        self.btn_recipe_value_finefiller_trickleweight_1_2.grid(row=_rpm_start_row + _rpm_act_row, column= _rpm_start_col + 4,  padx=_padsize_x, pady=_padsize_y)                          

        #parameter menue -> finefiller trickle weight quarter [1/4]

        _rpm_act_row += 1
        
        self.lbl_recipe_value_finefiller_trickleweight_1_4 = Label(master, text="trickle weight quarter - 1/4")
        self.lbl_recipe_value_finefiller_trickleweight_1_4.grid(row=_rpm_start_row + _rpm_act_row, column=_rpm_start_col + 0, padx=_padsize_x, pady=_padsize_y, sticky="w")

        self.lbl_recipe_value_finefiller_trickleweight_1_4_unit = Label(master, text="[mg]")
        self.lbl_recipe_value_finefiller_trickleweight_1_4_unit.grid(row=_rpm_start_row + _rpm_act_row, column= _rpm_start_col + 1,  padx=_padsize_x, pady=_padsize_y)
        
        self.txt_recipe_value_finefiller_trickleweight_1_4_hmi = Text(master, height=_txt_height, width=_txt_width)
        self.txt_recipe_value_finefiller_trickleweight_1_4_hmi.grid(row=_rpm_start_row + _rpm_act_row, column= _rpm_start_col + 2,  padx=_padsize_x, pady=_padsize_y)
        
        self.lbl_recipe_value_finefiller_trickleweight_1_4_esp = Label(master,bg="white", width=_txt_width, text="---")
        self.lbl_recipe_value_finefiller_trickleweight_1_4_esp.grid(row=_rpm_start_row + _rpm_act_row, column= _rpm_start_col + 3,  padx=_padsize_x, pady=_padsize_y)
        
        self.btn_recipe_value_finefiller_trickleweight_1_4 = Button(master,width=_txt_width,  text="download", command=self.qf_recipe_value_finefiller_trickleweight_1_4)
        self.btn_recipe_value_finefiller_trickleweight_1_4.grid(row=_rpm_start_row + _rpm_act_row, column= _rpm_start_col + 4,  padx=_padsize_x, pady=_padsize_y)                          

        #parameter menue -> finefiller trickle weight eigth [1/8]

        _rpm_act_row += 1
        
        self.lbl_recipe_value_finefiller_trickleweight_1_8 = Label(master, text="trickle weight eigth - 1/8")
        self.lbl_recipe_value_finefiller_trickleweight_1_8.grid(row=_rpm_start_row + _rpm_act_row, column=_rpm_start_col + 0, padx=_padsize_x, pady=_padsize_y, sticky="w")

        self.lbl_recipe_value_finefiller_trickleweight_1_8_unit = Label(master, text="[mg]")
        self.lbl_recipe_value_finefiller_trickleweight_1_8_unit.grid(row=_rpm_start_row + _rpm_act_row, column= _rpm_start_col + 1,  padx=_padsize_x, pady=_padsize_y)
        
        self.txt_recipe_value_finefiller_trickleweight_1_8_hmi = Text(master, height=_txt_height, width=_txt_width)
        self.txt_recipe_value_finefiller_trickleweight_1_8_hmi.grid(row=_rpm_start_row + _rpm_act_row, column= _rpm_start_col + 2,  padx=_padsize_x, pady=_padsize_y)
        
        self.lbl_recipe_value_finefiller_trickleweight_1_8_esp = Label(master,bg="white", width=_txt_width, text="---")
        self.lbl_recipe_value_finefiller_trickleweight_1_8_esp.grid(row=_rpm_start_row + _rpm_act_row, column= _rpm_start_col + 3,  padx=_padsize_x, pady=_padsize_y)
        
        self.btn_recipe_value_finefiller_trickleweight_1_8 = Button(master,width=_txt_width,  text="download", command=self.qf_recipe_value_finefiller_trickleweight_1_8)
        self.btn_recipe_value_finefiller_trickleweight_1_8.grid(row=_rpm_start_row + _rpm_act_row, column= _rpm_start_col + 4,  padx=_padsize_x, pady=_padsize_y)                          

        #parameter menue -> finefiller trickle weight sixteenth [1/16]

        _rpm_act_row += 1
        
        self.lbl_recipe_value_finefiller_trickleweight_1_16 = Label(master, text="trickle weight sixteenth - 1/16")
        self.lbl_recipe_value_finefiller_trickleweight_1_16.grid(row=_rpm_start_row + _rpm_act_row, column=_rpm_start_col + 0, padx=_padsize_x, pady=_padsize_y, sticky="w")

        self.lbl_recipe_value_finefiller_trickleweight_1_16_unit = Label(master, text="[mg]")
        self.lbl_recipe_value_finefiller_trickleweight_1_16_unit.grid(row=_rpm_start_row + _rpm_act_row, column= _rpm_start_col + 1,  padx=_padsize_x, pady=_padsize_y)
        
        self.txt_recipe_value_finefiller_trickleweight_1_16_hmi = Text(master, height=_txt_height, width=_txt_width)
        self.txt_recipe_value_finefiller_trickleweight_1_16_hmi.grid(row=_rpm_start_row + _rpm_act_row, column= _rpm_start_col + 2,  padx=_padsize_x, pady=_padsize_y)
        
        self.lbl_recipe_value_finefiller_trickleweight_1_16_esp = Label(master,bg="white", width=_txt_width, text="---")
        self.lbl_recipe_value_finefiller_trickleweight_1_16_esp.grid(row=_rpm_start_row + _rpm_act_row, column= _rpm_start_col + 3,  padx=_padsize_x, pady=_padsize_y)
        
        self.btn_recipe_value_finefiller_trickleweight_1_16 = Button(master,width=_txt_width,  text="download", command=self.qf_recipe_value_finefiller_trickleweight_1_16)
        self.btn_recipe_value_finefiller_trickleweight_1_16.grid(row=_rpm_start_row + _rpm_act_row, column= _rpm_start_col + 4,  padx=_padsize_x, pady=_padsize_y)                          


        #parameter menue -> finefiller bias

        _rpm_act_row += 1
        
        self.lbl_recipe_value_finefiller_bias = Label(master, text="bias")
        self.lbl_recipe_value_finefiller_bias.grid(row=_rpm_start_row + _rpm_act_row, column=_rpm_start_col + 0, padx=_padsize_x, pady=_padsize_y, sticky="w")

        self.lbl_recipe_value_finefiller_bias_unit = Label(master, text="[mg]")
        self.lbl_recipe_value_finefiller_bias_unit.grid(row=_rpm_start_row + _rpm_act_row, column= _rpm_start_col + 1,  padx=_padsize_x, pady=_padsize_y)
        
        self.txt_recipe_value_finefiller_bias_hmi = Text(master, height=_txt_height, width=_txt_width)
        self.txt_recipe_value_finefiller_bias_hmi.grid(row=_rpm_start_row + _rpm_act_row, column= _rpm_start_col + 2,  padx=_padsize_x, pady=_padsize_y)
        
        self.lbl_recipe_value_finefiller_bias_esp = Label(master,bg="white", width=_txt_width, text="---")
        self.lbl_recipe_value_finefiller_bias_esp.grid(row=_rpm_start_row + _rpm_act_row, column= _rpm_start_col + 3,  padx=_padsize_x, pady=_padsize_y)
        
        self.btn_recipe_value_finefiller_bias = Button(master,width=_txt_width,  text="download", command=self.qf_recipe_value_finefiller_bias)
        self.btn_recipe_value_finefiller_bias.grid(row=_rpm_start_row + _rpm_act_row, column= _rpm_start_col + 4,  padx=_padsize_x, pady=_padsize_y)

        #parameter menue -> finefiller max filling time

        _rpm_act_row += 1
        
        self.lbl_recipe_value_finefiller_max_filling_time = Label(master, text="max filling time")
        self.lbl_recipe_value_finefiller_max_filling_time.grid(row=_rpm_start_row + _rpm_act_row, column=_rpm_start_col + 0, padx=_padsize_x, pady=_padsize_y, sticky="w")

        self.lbl_recipe_value_finefiller_max_filling_time_unit = Label(master, text="[ms]")
        self.lbl_recipe_value_finefiller_max_filling_time_unit.grid(row=_rpm_start_row + _rpm_act_row, column= _rpm_start_col + 1,  padx=_padsize_x, pady=_padsize_y)
        
        self.txt_recipe_value_finefiller_max_filling_time_hmi = Text(master, height=_txt_height, width=_txt_width)
        self.txt_recipe_value_finefiller_max_filling_time_hmi.grid(row=_rpm_start_row + _rpm_act_row, column= _rpm_start_col + 2,  padx=_padsize_x, pady=_padsize_y)
        
        self.lbl_recipe_value_finefiller_max_filling_time_esp = Label(master,bg="white", width=_txt_width, text="---")
        self.lbl_recipe_value_finefiller_max_filling_time_esp.grid(row=_rpm_start_row + _rpm_act_row, column= _rpm_start_col + 3,  padx=_padsize_x, pady=_padsize_y)
        
        self.btn_recipe_value_finefiller_max_filling_time = Button(master,width=_txt_width,  text="download", command=self.qf_recipe_value_finefiller_max_filling_time)
        self.btn_recipe_value_finefiller_max_filling_time.grid(row=_rpm_start_row + _rpm_act_row, column= _rpm_start_col + 4,  padx=_padsize_x, pady=_padsize_y)
      
    ########################################################################################
    #places filling logger menue
    ########################################################################################

        _flm_start_row = 0
        _flm_start_col = _rpm_start_col + 7
        _flm_act_row = 0  

        _flm_txt_width_factor = 4
        self._flm_logger_size = 30

        self._flm_logger_avg_filling_time = []
        self._flm_logger_avg_filling_weight = []

        self.lbl_filling_logger_menue_header = Label(master,justify="left",anchor="w", width=_txt_width*_flm_txt_width_factor, text="filling logger menue:")
        self.lbl_filling_logger_menue_header.grid(row=_flm_start_row + _flm_act_row, column= _flm_start_col + 0,  padx=_padsize_x, pady=_padsize_y, sticky="w")
        
        _flm_act_row += 1

        self.lbl_filling_logger_menue_logger = Label(master,bg="white", anchor="nw",height= _txt_height*self._flm_logger_size, width=_txt_width*_flm_txt_width_factor, text="---\n"*self._flm_logger_size)
        self.lbl_filling_logger_menue_logger.grid(row=_flm_start_row + _flm_act_row, column= _flm_start_col + 0, rowspan=30, padx=_padsize_x, pady=_padsize_y, sticky="n")
        
        _flm_act_row += 18

        self.lbl_filling_logger_filling_number = Label(master,anchor="w", width=_txt_width*_flm_txt_width_factor, text="Number of fillings: ---")
        self.lbl_filling_logger_filling_number.grid(row=_flm_start_row + _flm_act_row, column= _flm_start_col + 0,  padx=_padsize_x, pady=_padsize_y, sticky="w")
        

        _flm_act_row += 1

        self.lbl_filling_logger_avg_filling_time = Label(master,anchor="w", width=_txt_width*_flm_txt_width_factor, text="Average filling time: ---")
        self.lbl_filling_logger_avg_filling_time.grid(row=_flm_start_row + _flm_act_row, column= _flm_start_col + 0,  padx=_padsize_x, pady=_padsize_y, sticky="w")
        
        _flm_act_row += 1

        self.lbl_filling_logger_avg_filling_weight = Label(master,anchor="w", width=_txt_width*_flm_txt_width_factor, text="Average filling weight: ---")
        self.lbl_filling_logger_avg_filling_weight.grid(row=_flm_start_row + _flm_act_row, column= _flm_start_col + 0,  padx=_padsize_x, pady=_padsize_y, sticky="w")
       
    ########################################################################################
    #functions mqtt stuff
    ########################################################################################

    def on_connect(self, client, userdata, flags, rc):
        print("Connected with result code " + str(rc))
        self.client.subscribe("#", 0)

        #Available on esp side
        #mosquitto_pub -h 0.0.0.0 -t /dev1/trickler/display -m 
        #mosquitto_pub -h 0.0.0.0 -t /dev1/trickler/esp/progress -m
        #mosquitto_pub -h 0.0.0.0 -t /dev1/trickler/esp/scale_connection_state -m
        #mosquitto_pub -h 0.0.0.0 -t /dev0/scale/current_weight -m
        #mosquitto_pub -h 0.0.0.0 -t /dev1/trickler/alive -m
        #mosquitto_pub -h 0.0.0.0 -t /dev0/scale/alive -m
        #mosquitto_pub -h 0.0.0.0 -t /dev1/trickler/uploadId -m
        #mosquitto_pub -h 0.0.0.0 -t /dev1/trickler/esp/realtimeplotter -m
        
        #New on esp
        #mosquitto_pub -h 0.0.0.0 -t  /dev1/trickler/active_prameter/filling_weight  -m
        #mosquitto_pub -h 0.0.0.0 -t  /dev1/trickler/active_prameter/prefiller_velocity_fast  -m
        #mosquitto_pub -h 0.0.0.0 -t  /dev1/trickler/active_prameter/prefiller_weight_slow  -m
        #mosquitto_pub -h 0.0.0.0 -t  /dev1/trickler/active_prameter/prefiller_weight_stop  -m
        #mosquitto_pub -h 0.0.0.0 -t  /dev1/trickler/active_prameter/prefiller_velocity_slow  -m
        #mosquitto_pub -h 0.0.0.0 -t  /dev1/trickler/active_prameter/prefiller_handle_finefiller  -m
        #mosquitto_pub -h 0.0.0.0 -t  /dev1/trickler/active_prameter/finefiller_velocity_1_1  -m
        #mosquitto_pub -h 0.0.0.0 -t  /dev1/trickler/active_prameter/finefiller_velocity_1_2  -m
        #mosquitto_pub -h 0.0.0.0 -t  /dev1/trickler/active_prameter/finefiller_velocity_1_4  -m
        #mosquitto_pub -h 0.0.0.0 -t  /dev1/trickler/active_prameter/finefiller_velocity_1_8  -m
        #mosquitto_pub -h 0.0.0.0 -t  /dev1/trickler/active_prameter/finefiller_velocity_1_16  -m
        #mosquitto_pub -h 0.0.0.0 -t  /dev1/trickler/active_prameter/finefiller_trickle_weight_1_2  -m
        #mosquitto_pub -h 0.0.0.0 -t  /dev1/trickler/active_prameter/finefiller_trickle_weight_1_4  -m
        #mosquitto_pub -h 0.0.0.0 -t  /dev1/trickler/active_prameter/finefiller_trickle_weight_1_8  -m
        #mosquitto_pub -h 0.0.0.0 -t  /dev1/trickler/active_prameter/finefiller_trickle_weight_1_16  -m
        #mosquitto_pub -h 0.0.0.0 -t  /dev1/trickler/active_prameter/finefiller_bias  -m
        #mosquitto_pub -h 0.0.0.0 -t  /dev1/trickler/active_prameter/finefiller_max_filling_time  -m



        self.client.message_callback_add("/dev1/trickler/display", self.on_message_display)
        self.client.message_callback_add("/dev1/trickler/esp/progress", self.on_message_progress)
        self.client.message_callback_add("/dev1/trickler/esp/scale_connection_state", self.on_message_connection_state)
        self.client.message_callback_add("/dev0/scale/current_weight", self.on_message_scale_weight)
        self.client.message_callback_add("/dev1/trickler/alive", self.on_message_esp_alive)
        self.client.message_callback_add("/dev0/scale/alive", self.on_message_open_scale_alive)
        self.client.message_callback_add("/dev1/trickler/uploadId", self.on_message_upload_id)
        self.client.message_callback_add("/dev1/trickler/esp/realtimeplotter", self.on_message_plotter)
        
        
        self.client.message_callback_add("/dev1/trickler/active_prameter/filling_weight", self.on_message_active_prameter_filling_weight)
        self.client.message_callback_add("/dev1/trickler/active_prameter/prefiller_velocity_fast", self.on_message_active_prameter_prefiller_velocity_fast)
        self.client.message_callback_add("/dev1/trickler/active_prameter/prefiller_weight_slow", self.on_message_active_prameter_prefiller_weight_slow)
        self.client.message_callback_add("/dev1/trickler/active_prameter/prefiller_velocity_slow", self.on_message_active_prameter_prefiller_velocity_slow)
        self.client.message_callback_add("/dev1/trickler/active_prameter/prefiller_weight_stop", self.on_message_active_prameter_prefiller_weight_stop)
        self.client.message_callback_add("/dev1/trickler/active_prameter/prefiller_handle_finefiller", self.on_message_active_prameter_prefiller_handle_finefiller)
        self.client.message_callback_add("/dev1/trickler/active_prameter/finefiller_velocity_1_1", self.on_message_active_prameter_finefiller_velocity_1_1)
        self.client.message_callback_add("/dev1/trickler/active_prameter/finefiller_velocity_1_2", self.on_message_active_prameter_finefiller_velocity_1_2)
        self.client.message_callback_add("/dev1/trickler/active_prameter/finefiller_velocity_1_4", self.on_message_active_prameter_finefiller_velocity_1_4)
        self.client.message_callback_add("/dev1/trickler/active_prameter/finefiller_velocity_1_8", self.on_message_active_prameter_finefiller_velocity_1_8)
        self.client.message_callback_add("/dev1/trickler/active_prameter/finefiller_velocity_1_16", self.on_message_active_prameter_finefiller_velocity_1_16)
        self.client.message_callback_add("/dev1/trickler/active_prameter/finefiller_trickle_weight_1_2", self.on_message_active_prameter_finefiller_trickle_weight_1_2)
        self.client.message_callback_add("/dev1/trickler/active_prameter/finefiller_trickle_weight_1_4", self.on_message_active_prameter_finefiller_trickle_weight_1_4)
        self.client.message_callback_add("/dev1/trickler/active_prameter/finefiller_trickle_weight_1_8", self.on_message_active_prameter_finefiller_trickle_weight_1_8)
        self.client.message_callback_add("/dev1/trickler/active_prameter/finefiller_trickle_weight_1_16", self.on_message_active_prameter_finefiller_trickle_weight_1_16)
        self.client.message_callback_add("/dev1/trickler/active_prameter/finefiller_bias", self.on_message_active_prameter_finefiller_bias)
        self.client.message_callback_add("/dev1/trickler/active_prameter/finefiller_max_filling_time", self.active_prameter_finefiller_max_filling_time)

        #self.client.message_callback_add("", self.on_message_)



    def on_message_display(self, client, userdata, msg):
        self.lbl_control_menue_display["text"]="Display:\n" + self._filterMqttMsg(msg.payload)

    def on_message_connection_state(self, client, userdata, msg):        
        if self._filterMqttMsg(msg.payload) == "1":
            self.lbl_control_menue_open_scale_connection.config(bg="green")
        else:
            self.lbl_control_menue_open_scale_connection.config(bg="red")

    def on_message_progress(self, client, userdata, msg):
        self.prgb_control_menue_progress["value"] = self._filterMqttMsg(msg.payload)

    def on_message_scale_weight(self, client, userdata, msg):
        self.lbl_scale_menue_weight["text"]= "weight: " + self._filterMqttMsg(msg.payload)

    def on_message_esp_alive(self, client, userdata, msg):
        self.lbl_debug_menue_esp_alive["text"]="ESP Alive: " + self._filterMqttMsg(msg.payload)

    def on_message_open_scale_alive(self, client, userdata, msg):
        self.lbl_debug_menue_openscale_alive["text"]="Open Scale Alive: " + self._filterMqttMsg(msg.payload)

    def on_message_upload_id(self, client, userdata, msg):
        self.lbl_recipe_save_menue_upload_id["text"] = self._filterMqttMsg(msg.payload)

    def on_message_plotter(self, client, userdata, msg):

        old_logger_entry = str(self.lbl_filling_logger_menue_logger["text"]).split("\n")
        
        num, time, weight = str(self._filterMqttMsg(msg.payload)).split(";")
        
        space = " "*12
        
        new_logger_entry=(str(num) + space + str(time) + " ms" + space + str(weight) + " mg"   +"\n")
     

        for i in range(0,self._flm_logger_size+1):
            new_logger_entry += old_logger_entry[i] + "\n"

        self.lbl_filling_logger_menue_logger["text"] = new_logger_entry
       
        self._flm_logger_avg_filling_time.append(int(time))
        self._flm_logger_avg_filling_weight.append(int(weight))

        if len(self._flm_logger_avg_filling_time) > self._flm_logger_size:
            self._flm_logger_avg_filling_time[-1*self._flm_logger_size]

        if len(self._flm_logger_avg_filling_weight) > self._flm_logger_size:
            self._flm_logger_avg_filling_weight[-1*self._flm_logger_size]

        self.lbl_filling_logger_filling_number["text"] = "Number of fillings: " +  num
        self.lbl_filling_logger_avg_filling_time["text"] = "Average filling time: " + str(self._CalculateAverage(self._flm_logger_avg_filling_time)) + " ms"
        self.lbl_filling_logger_avg_filling_weight["text"] = "Average filling weight: " + str(self._CalculateAverage(self._flm_logger_avg_filling_weight)) + " mg"
           
    def on_message_active_prameter_filling_weight(self, client, userdata, msg):
        self.lbl_recipe_value_filling_weight_esp["text"]= self._filterMqttMsg(msg.payload)
    
    def on_message_active_prameter_prefiller_velocity_fast(self, client, userdata, msg):
        self.lbl_recipe_value_prefiller_velocity_fast_esp["text"]= self._filterMqttMsg(msg.payload)

    def on_message_active_prameter_prefiller_weight_slow(self, client, userdata, msg):
        self.lbl_recipe_value_prefiller_weight_slow_esp["text"]= self._filterMqttMsg(msg.payload)

    def on_message_active_prameter_prefiller_velocity_slow(self, client, userdata, msg):
        self.lbl_recipe_value_prefiller_velocity_slow_esp["text"]= self._filterMqttMsg(msg.payload)

    def on_message_active_prameter_prefiller_weight_stop(self, client, userdata, msg):
        self.lbl_recipe_value_prefiller_weight_stop_esp["text"]= self._filterMqttMsg(msg.payload)

    def on_message_active_prameter_prefiller_handle_finefiller(self, client, userdata, msg):
        if self._filterMqttMsg(msg.payload) == "1":
            self.lbl_recipe_value_prefiller_handle_finefiller_esp["text"]= "enable"
        else:
            self.lbl_recipe_value_prefiller_handle_finefiller_esp["text"]= "disable"

    def on_message_active_prameter_finefiller_velocity_1_1(self, client, userdata, msg):
        self.lbl_recipe_value_finefiller_velocity_1_1_esp["text"]= self._filterMqttMsg(msg.payload)

    def on_message_active_prameter_finefiller_velocity_1_2(self, client, userdata, msg):
        self.lbl_recipe_value_finefiller_velocity_1_2_esp["text"]= self._filterMqttMsg(msg.payload)

    def on_message_active_prameter_finefiller_velocity_1_4(self, client, userdata, msg):
        self.lbl_recipe_value_finefiller_velocity_1_4_esp["text"]= self._filterMqttMsg(msg.payload)

    def on_message_active_prameter_finefiller_velocity_1_8(self, client, userdata, msg):
        self.lbl_recipe_value_finefiller_velocity_1_8_esp["text"]= self._filterMqttMsg(msg.payload)

    def on_message_active_prameter_finefiller_velocity_1_16(self, client, userdata, msg):
        self.lbl_recipe_value_finefiller_velocity_1_16_esp["text"]= self._filterMqttMsg(msg.payload)

    def on_message_active_prameter_finefiller_trickle_weight_1_2(self, client, userdata, msg):
        self.lbl_recipe_value_finefiller_trickleweight_1_2_esp["text"]= self._filterMqttMsg(msg.payload)

    def on_message_active_prameter_finefiller_trickle_weight_1_4(self, client, userdata, msg):
        self.lbl_recipe_value_finefiller_trickleweight_1_4_esp["text"]= self._filterMqttMsg(msg.payload)

    def on_message_active_prameter_finefiller_trickle_weight_1_8(self, client, userdata, msg):
        self.lbl_recipe_value_finefiller_trickleweight_1_8_esp["text"]= self._filterMqttMsg(msg.payload)

    def on_message_active_prameter_finefiller_trickle_weight_1_16(self, client, userdata, msg):
        self.lbl_recipe_value_finefiller_trickleweight_1_16_esp["text"]= self._filterMqttMsg(msg.payload)

    def on_message_active_prameter_finefiller_bias(self, client, userdata, msg):
        self.lbl_recipe_value_finefiller_bias_esp["text"]= self._filterMqttMsg(msg.payload)

    def active_prameter_finefiller_max_filling_time(self, client, userdata, msg):
        self.lbl_recipe_value_finefiller_max_filling_time_esp["text"]= self._filterMqttMsg(msg.payload)

    def on_message(self, client, userdata, msg):
        pass
        
    ########################################################################################
    #functions control menue
    ########################################################################################

    def qf_oplst_control_menue_opmode(self, *args):
        
        topic = "/dev1/trickler/recipe/set_mode"
        payload = self._var_oplst_control_menue_opmode.get()
       
        if payload == "Automatic Action Mode":
            payload = "0"
        elif payload == "Single Action Mode":
            payload = "1"
        elif payload == "Setting Mode - FineFiller":
            payload = "2"
        elif payload == "Setting Mode - PreFiller":
            payload = "3"
        else:
            payload = "0"      

        self.client.publish(topic=topic, payload=payload,qos=self._qos, retain=False)

    def qf_oplst_control_menue_setting_finefiller(self, *args):
        
        topic = "/dev1/trickler/recipe/FineFillerSettingMode"
        payload = self._var_oplst_control_menue_setting_finefiller.get()

        if payload == "disabled":
            payload = "0"
        elif payload == "1/1 Stepmode":
            payload = "1"
        elif payload == "1/2 Stepmode":
            payload = "2"
        elif payload == "1/4 Stepmode":
            payload = "3"
        elif payload == "1/8 Stepmode":
            payload = "4"
        elif payload == "1/16 Stepmode":
            payload = "5"
        else:
            payload = "0"                                     

        self.client.publish(topic=topic, payload=payload,qos=self._qos, retain=False)

    def qf_control_menue_single_action(self):
        
        topic = "/dev1/trickler/button/single_action"
        payload = ""

        self.client.publish(topic=topic, payload=payload,qos=self._qos, retain=False)

    def qf_control_menue_abort(self):
        
        topic = "/dev1/trickler/button/abort"
        payload = ""

        self.client.publish(topic=topic, payload=payload,qos=self._qos, retain=False)

    
    ########################################################################################
    #functions scale menue
    ########################################################################################

    def qf_scale_menue_tare(self):
        
        topic = "/dev1/trickler/esp/scale_request_tare"
        payload = ""

        self.client.publish(topic=topic, payload=payload,qos=self._qos, retain=False)
   
    def qf_scale_menue_connect(self):
        
        #select active user
        if user == "pi":
            tool = "OpenScaleWrapper.py"
        else:
            tool = "OpenScaleWrapper_.py"
        

        os.system("pkill -f " + tool)
        os.system("python " + self._path_openscale + tool + " &")

    ########################################################################################
    #functions flash menue
    ########################################################################################

    def qf_flash_menue_load_file(self):

        savepopup = filedialog.askopenfilename(initialdir = "/home/$USER/Desktop",title = "Load firmware file",filetypes = (("binary","*.bin"),("all files","*.*")))
        
        #check if string is not empty
        if savepopup != "":
            self.lbl_flash_menue_filepath["text"]=savepopup
        else:
            self.lbl_flash_menue_filepath["text"]="---"
        
    def qf_flash_menue_flash_esp(self):       

        port= "--port " + self._var_oplst_flash_menue_ports.get()
        baud= "--baud " + self._var_oplst_flash_menue_baud.get()
        filepath= self.lbl_flash_menue_filepath["text"]

        #esptool.py --port COM3 --baud 115200 write_flash -fm dio -fs 32m 0x00000 ESPEasy_R120_4096.bin
        cmd="esptool.py " + port  + " " +  baud + " write_flash -fm dio -fs 32m 0x00000 " +filepath

        os.system(cmd)            


    ########################################################################################
    #functions recipe save menue
    ########################################################################################
    
    #function: load active parameter from textfile   
    def qf_save_menue_load(self):

        result = messagebox.askokcancel("Load Recipe","Do you want to overwrite active Recipe on hmi ?")       
        
        #true if butt0n ok is pressed
        if result:

            f = open(self._path_recipes +  self._var_oplst_recipe_save_menue.get(), "r")

            lines = f.readlines()

            for line in lines:
                
                if line.find("#!") != -1:
                    param = line.split(":",1)
                    
                    tag = param[0].replace("#!","")
                    value = param[1].replace("\n","")
                    value = value.replace(" ", "",100)

                    if tag == "FILLING_WEIGHT":
                        self.txt_recipe_value_filling_weight_hmi.delete(1.0,"end")
                        self.txt_recipe_value_filling_weight_hmi.insert(1.0,value)

                    elif tag == "PREFILLER_VELOCITY_FAST":
                        self.txt_recipe_value_prefiller_velocity_fast_hmi.delete(1.0,"end")
                        self.txt_recipe_value_prefiller_velocity_fast_hmi.insert(1.0,value)

                    elif tag == "PREFILLER_WEIGHT_SLOW":
                        self.txt_recipe_value_prefiller_weight_slow_hmi.delete(1.0,"end")
                        self.txt_recipe_value_prefiller_weight_slow_hmi.insert(1.0,value)

                    elif tag == "PREFILLER_VELOCITY_SLOW":
                        self.txt_recipe_value_prefiller_velocity_slow_hmi.delete(1.0,"end")
                        self.txt_recipe_value_prefiller_velocity_slow_hmi.insert(1.0,value)

                    elif tag == "PREFILLER_WEIGHT_STOP":
                        self.txt_recipe_value_prefiller_weight_stop_hmi.delete(1.0,"end")
                        self.txt_recipe_value_prefiller_weight_stop_hmi.insert(1.0,value)

                    elif tag == "FINEILLER_VELOCITY_FULL":
                        self.txt_recipe_value_finefiller_velocity_1_1_hmi.delete(1.0,"end")
                        self.txt_recipe_value_finefiller_velocity_1_1_hmi.insert(1.0,value)

                    elif tag == "FINEILLER_VELOCITY_HALF":
                        self.txt_recipe_value_finefiller_velocity_1_2_hmi.delete(1.0,"end")
                        self.txt_recipe_value_finefiller_velocity_1_2_hmi.insert(1.0,value)

                    elif tag == "FINEILLER_VELOCITY_QUARTER":
                        self.txt_recipe_value_finefiller_velocity_1_4_hmi.delete(1.0,"end")
                        self.txt_recipe_value_finefiller_velocity_1_4_hmi.insert(1.0,value)

                    elif tag == "FINEILLER_VELOCITY_EIGTH":
                        self.txt_recipe_value_finefiller_velocity_1_8_hmi.delete(1.0,"end")
                        self.txt_recipe_value_finefiller_velocity_1_8_hmi.insert(1.0,value)
        
                    elif tag == "FINEILLER_VELOCITY_SIXTEENTH":
                        self.txt_recipe_value_finefiller_velocity_1_16_hmi.delete(1.0,"end")
                        self.txt_recipe_value_finefiller_velocity_1_16_hmi.insert(1.0,value)

                    elif tag == "FINEILLER_TRICKLEWEIGHT_HALF":
                        self.txt_recipe_value_finefiller_trickleweight_1_2_hmi.delete(1.0,"end")
                        self.txt_recipe_value_finefiller_trickleweight_1_2_hmi.insert(1.0,value)

                    elif tag == "FINEILLER_TRICKLEWEIGHT_QUARTER":
                        self.txt_recipe_value_finefiller_trickleweight_1_4_hmi.delete(1.0,"end")
                        self.txt_recipe_value_finefiller_trickleweight_1_4_hmi.insert(1.0,value)

                    elif tag == "FINEILLER_TRICKLEWEIGHT_EIGTH":
                        self.txt_recipe_value_finefiller_trickleweight_1_8_hmi.delete(1.0,"end")
                        self.txt_recipe_value_finefiller_trickleweight_1_8_hmi.insert(1.0,value)

                    elif tag == "FINEILLER_TRICKLEWEIGHT_SIXTEENTH":
                        self.txt_recipe_value_finefiller_trickleweight_1_16_hmi.delete(1.0,"end")
                        self.txt_recipe_value_finefiller_trickleweight_1_16_hmi.insert(1.0,value)

                    elif tag == "FINEILLER_BIAS":
                        self.txt_recipe_value_finefiller_bias_hmi.delete(1.0,"end")
                        self.txt_recipe_value_finefiller_bias_hmi.insert(1.0,value)

                    elif tag == "FINEILLER_VELOCITY_MAX_FILLING_TIME":
                        self.txt_recipe_value_finefiller_max_filling_time_hmi.delete(1.0,"end")
                        self.txt_recipe_value_finefiller_max_filling_time_hmi.insert(1.0,value)

            f.close()

    #function: save active parameter to textfile   
    def qf_save_menue_save(self):

        #Important: 
        #regular comment  -> #Comment
        #recipe parameter -> #!name:value

        result = messagebox.askokcancel("Save Recipe","Do you want to overwrite Recipe \"" +  self._var_oplst_recipe_save_menue.get() + "\" ?")       
        
        #true if button ok is pressed
        if result:
            #open selected recipe file    
            f = open(self._path_recipes +  self._var_oplst_recipe_save_menue.get(), "w")

            #write header
            f.write("##################################" + "\n") 
            f.write("#recipe name: " + self._var_oplst_recipe_save_menue.get() + "\n")
            f.write("#recipe timestamp: " + self._funcGetTimestamp() + "\n")
            f.write("##################################" + "\n") 
            f.write("\n")

            #save general parameter
            f.write("#!FILLING_WEIGHT:"  + self.txt_recipe_value_filling_weight_hmi.get(1.0,"end"))

            f.write("\n")

            #save prefiller parameter
            f.write("#!PREFILLER_VELOCITY_FAST:"  + self.txt_recipe_value_prefiller_velocity_fast_hmi.get(1.0,"end"))        
            f.write("#!PREFILLER_WEIGHT_SLOW:"  + self.txt_recipe_value_prefiller_weight_slow_hmi.get(1.0,"end"))        
            f.write("#!PREFILLER_VELOCITY_SLOW:" + self.txt_recipe_value_prefiller_velocity_slow_hmi.get(1.0,"end"))     
            f.write("#!PREFILLER_WEIGHT_STOP:"  + self.txt_recipe_value_prefiller_weight_stop_hmi.get(1.0,"end"))           
            #f.write("#!PREFILLER_HANDLE_finefiller:" + "n.a.")        

            f.write("\n")

            #save finefiller parameter
            f.write("#!FINEILLER_VELOCITY_FULL:"  + self.txt_recipe_value_finefiller_velocity_1_1_hmi.get(1.0,"end"))        
            f.write("#!FINEILLER_VELOCITY_HALF:"  + self.txt_recipe_value_finefiller_velocity_1_2_hmi.get(1.0,"end"))        
            f.write("#!FINEILLER_VELOCITY_QUARTER:"  + self.txt_recipe_value_finefiller_velocity_1_4_hmi.get(1.0,"end"))
            f.write("#!FINEILLER_VELOCITY_EIGTH:"  + self.txt_recipe_value_finefiller_velocity_1_8_hmi.get(1.0,"end"))        
            f.write("#!FINEILLER_VELOCITY_SIXTEENTH:" + self.txt_recipe_value_finefiller_velocity_1_16_hmi.get(1.0,"end"))        
            f.write("#!FINEILLER_TRICKLEWEIGHT_HALF:"  + self.txt_recipe_value_finefiller_trickleweight_1_2_hmi.get(1.0,"end"))
            f.write("#!FINEILLER_TRICKLEWEIGHT_QUARTER:"  + self.txt_recipe_value_finefiller_trickleweight_1_4_hmi.get(1.0,"end"))
            f.write("#!FINEILLER_TRICKLEWEIGHT_EIGTH:"  + self.txt_recipe_value_finefiller_trickleweight_1_8_hmi.get(1.0,"end"))
            f.write("#!FINEILLER_TRICKLEWEIGHT_SIXTEENTH:"  + self.txt_recipe_value_finefiller_trickleweight_1_16_hmi.get(1.0,"end"))

            f.write("#!FINEILLER_BIAS:" + self.txt_recipe_value_finefiller_bias_hmi.get(1.0,"end"))
            f.write("#!FINEILLER_VELOCITY_MAX_FILLING_TIME:" + self.txt_recipe_value_finefiller_max_filling_time_hmi.get(1.0,"end"))        
            
            f.close()
        
    def qf_save_menue_default(self):
        
        topic = "/dev1/trickler/button/save_config"
        payload = ""
        
        self.client.publish(topic=topic, payload=payload,qos=self._qos, retain=False)

    #function: create new parameter textfile            
    def qf_save_menue_create(self):
        
        #open popup window
        savepopup = filedialog.asksaveasfilename(initialdir = self._path_recipes,title = "Save new Recipe",filetypes = (("all files","**"),("all files","*.*")))
        
        #check if string is not empty
        if savepopup != "":

            #create new (empty) recipe file
            f = open(savepopup, "a")
            f.close
            
            #read new filelist
            filenames = os.listdir(self._path_recipes)
            self.oplst_recipe_save_menue['menu'].delete(0, 'end')
            
            #add new file to option menue -> save menue
            for name in filenames:
                self.oplst_recipe_save_menue['menu'].add_command(label=name, command=lambda name=name: self._var_oplst_recipe_save_menue.set(name))

    #function downloads all recipe values
    def qf_save_menue_download_all(self):
         
        #define sleeptime between parameter transfer
        sleeptime = 0.00

        self.qf_recipe_value_filling_weight()
        sleep(sleeptime)

        self.qf_recipe_value_prefiller_velocity_fast()
        sleep(sleeptime)

        self.qf_recipe_value_prefiller_weight_slow()
        sleep(sleeptime)

        self.qf_recipe_value_prefiller_velocity_slow()
        sleep(sleeptime)

        self.qf_recipe_value_prefiller_weight_stop()
        sleep(sleeptime)

        self.qf_recipe_value_prefiller_handle_finefiller()
        sleep(sleeptime)

        self.qf_recipe_value_finefiller_velocity_1_1()
        sleep(sleeptime)

        self.qf_recipe_value_finefiller_velocity_1_2()
        sleep(sleeptime)

        self.qf_recipe_value_finefiller_velocity_1_4()      
        sleep(sleeptime)

        self.qf_recipe_value_finefiller_velocity_1_8()
        sleep(sleeptime)

        self.qf_recipe_value_finefiller_velocity_1_16()
        sleep(sleeptime)
        
        self.qf_recipe_value_finefiller_trickleweight_1_2()
        sleep(sleeptime)

        self.qf_recipe_value_finefiller_trickleweight_1_4()
        sleep(sleeptime)

        self.qf_recipe_value_finefiller_trickleweight_1_8()
        sleep(sleeptime)

        self.qf_recipe_value_finefiller_trickleweight_1_16()
        sleep(sleeptime)
        
        self.qf_recipe_value_finefiller_bias()
        sleep(sleeptime)

        self.qf_recipe_value_finefiller_max_filling_time()
        
    ########################################################################################
    #functions recipe parameter menue
    ########################################################################################
    
    def qf_recipe_value_filling_weight(self):
        
        topic = "/dev1/trickler/recipe/filling_weight"
        payload = self.txt_recipe_value_filling_weight_hmi.get(1.0,"end")
        payload.replace(" ","",100)

        if payload == "":
            payload = "0"

        self.client.publish(topic=topic, payload=payload,qos=self._qos, retain=False)
        
    def qf_recipe_value_prefiller_velocity_fast(self):
        
        topic = "/dev1/trickler/recipe/prefiller_velocity_fast"
        payload = self.txt_recipe_value_prefiller_velocity_fast_hmi.get(1.0,"end")
        payload.replace(" ","",100)

        if payload == "":
            payload = "0"

        self.client.publish(topic=topic, payload=payload,qos=self._qos, retain=False)
        
    def qf_recipe_value_prefiller_weight_slow(self):
        
        topic = "/dev1/trickler/recipe/prefiller_weight_slow"
        payload = self.txt_recipe_value_prefiller_weight_slow_hmi.get(1.0,"end")
        payload.replace(" ","",100)

        if payload == "":
            payload = "0"

        self.client.publish(topic=topic, payload=payload,qos=self._qos, retain=False)
    
    def qf_recipe_value_prefiller_velocity_slow(self):
        
        topic = "/dev1/trickler/recipe/prefiller_velocity_slow"
        payload = self.txt_recipe_value_prefiller_velocity_slow_hmi.get(1.0,"end")
        payload.replace(" ","",100)

        if payload == "":
            payload = "0"

        self.client.publish(topic=topic, payload=payload,qos=self._qos, retain=False)

    def qf_recipe_value_prefiller_weight_stop(self):
        
        topic = "/dev1/trickler/recipe/prefiller_weight_stop"
        payload = self.txt_recipe_value_prefiller_weight_stop_hmi.get(1.0,"end")
        payload.replace(" ","",100)

        if payload == "":
            payload = "0"

        self.client.publish(topic=topic, payload=payload,qos=self._qos, retain=False)
    
    def qf_recipe_value_prefiller_handle_finefiller(self):
        
        topic = "/dev1/trickler/recipe/prefiller_finefillermode"
        payload = self._var_oplst_recipe_value_prefiller_handle_finefiller.get()

        if payload == "enable":
            payload = "1"
        else:
            payload = "0"

        self.client.publish(topic=topic, payload=payload,qos=self._qos, retain=False)

    def qf_recipe_value_finefiller_velocity_1_1(self):
        
        topic = "/dev1/trickler/recipe/finefiller_velocity_full"
        payload = self.txt_recipe_value_finefiller_velocity_1_1_hmi.get(1.0,"end")
        payload.replace(" ","",100)

        if payload == "":
            payload = "0"

        self.client.publish(topic=topic, payload=payload,qos=self._qos, retain=False)
        
    def qf_recipe_value_finefiller_velocity_1_2(self):
        
        topic = "/dev1/trickler/recipe/finefiller_velocity_half"
        payload = self.txt_recipe_value_finefiller_velocity_1_2_hmi.get(1.0,"end")
        payload.replace(" ","",100)

        if payload == "":
            payload = "0"

        self.client.publish(topic=topic, payload=payload,qos=self._qos, retain=False)

    def qf_recipe_value_finefiller_velocity_1_4(self):
        
        topic = "/dev1/trickler/recipe/finefiller_velocity_quarter"
        payload = self.txt_recipe_value_finefiller_velocity_1_4_hmi.get(1.0,"end")
        payload.replace(" ","",100)

        if payload == "":
            payload = "0"

        self.client.publish(topic=topic, payload=payload,qos=self._qos, retain=False)

    def qf_recipe_value_finefiller_velocity_1_8(self):
        
        topic = "/dev1/trickler/recipe/finefiller_velocity_eigth"
        payload = self.txt_recipe_value_finefiller_velocity_1_8_hmi.get(1.0,"end")
        payload.replace(" ","",100)

        if payload == "":
            payload = "0"

        self.client.publish(topic=topic, payload=payload,qos=self._qos, retain=False)

    def qf_recipe_value_finefiller_velocity_1_16(self):
        
        topic = "/dev1/trickler/recipe/finefiller_velocity_sixteenth"
        payload = self.txt_recipe_value_finefiller_velocity_1_16_hmi.get(1.0,"end")
        payload.replace(" ","",100)

        if payload == "":
            payload = "0"

        self.client.publish(topic=topic, payload=payload,qos=self._qos, retain=False)

    def qf_recipe_value_finefiller_trickleweight_1_2(self):
        
        topic = "/dev1/trickler/recipe/finefiller_trickleweight_half"
        payload = self.txt_recipe_value_finefiller_trickleweight_1_2_hmi.get(1.0,"end")
        payload.replace(" ","",100)

        if payload == "":
            payload = "0"

        self.client.publish(topic=topic, payload=payload,qos=self._qos, retain=False)

    def qf_recipe_value_finefiller_trickleweight_1_4(self):
        
        topic = "/dev1/trickler/recipe/finefiller_trickleweight_quarter"
        payload = self.txt_recipe_value_finefiller_trickleweight_1_4_hmi.get(1.0,"end")
        payload.replace(" ","",100)

        if payload == "":
            payload = "0"

        self.client.publish(topic=topic, payload=payload,qos=self._qos, retain=False)

    def qf_recipe_value_finefiller_trickleweight_1_8(self):
        
        topic = "/dev1/trickler/recipe/finefiller_trickleweight_eigth"
        payload = self.txt_recipe_value_finefiller_trickleweight_1_8_hmi.get(1.0,"end")
        payload.replace(" ","",100)

        if payload == "":
            payload = "0"

        self.client.publish(topic=topic, payload=payload,qos=self._qos, retain=False)

    def qf_recipe_value_finefiller_trickleweight_1_16(self):
        
        topic = "/dev1/trickler/recipe/finefiller_trickleweight_sixteenth"
        payload = self.txt_recipe_value_finefiller_trickleweight_1_16_hmi.get(1.0,"end")
        payload.replace(" ","",100)

        if payload == "":
            payload = "0"

        self.client.publish(topic=topic, payload=payload,qos=self._qos, retain=False)

    def qf_recipe_value_finefiller_bias(self):
        
        topic = "/dev1/trickler/recipe/finefiller_bias"
        payload = self.txt_recipe_value_finefiller_bias_hmi.get(1.0,"end")
        payload.replace(" ","",100)

        if payload == "":
            payload = "0"

        self.client.publish(topic=topic, payload=payload,qos=self._qos, retain=False)
       
    def qf_recipe_value_finefiller_max_filling_time(self):
        
        topic = "/dev1/trickler/recipe/finefiller_max_filling_time"
        payload = self.txt_recipe_value_finefiller_max_filling_time_hmi.get(1.0,"end")
        payload.replace(" ","",100)

        if payload == "":
            payload = "0"

        self.client.publish(topic=topic, payload=payload,qos=self._qos, retain=False)

    ########################################################################################
    #class internal functions 
    ########################################################################################

    #function returns current timestamp
    def _funcGetTimestamp(self):
        return datetime.now().strftime("%d/%m/%Y %H:%M:%S")

    def _filterMqttMsg(self, payload):
        #Payload is defined as bytecode: "b'payload'"        
        lst=str(payload).split("'",2)
        return lst[1]

    def _CalculateAverage(self, lst): 
        avg = 0
        i = 0
        for val in lst:
            avg = avg + val
            i = i+1
        
        if i > 0:
            avg = avg / i
        else:
            avg = 0

        return round(avg, 3)


    def _logger(self, entry, elements):
        pass
        #logentry = self._funcGetTimestamp + ":     " + entry + "     "
        #for element in elements:
        #    logentry += element + ";     "



root = Tk()
my_gui = OpenTricklerUi(root)
root.mainloop()

