#!/usr/bin/env python
# -*- coding: utf-8 -*-

#V1.1 modify for raspberry pi with local host mqtt broker 

import serial
import paho.mqtt.client as mqtt
import time
#import netifaces

####################################################################################################
# Definiton of functions
####################################################################################################

def on_connect(client, userdata, flags, rc):
    print("Connected with result code " + str(rc))
    client.subscribe("/dev1/trickler/esp/scale_request_tare", 0)

def on_message(client, userdata, msg):  # The callback for when a PUBLISH message 
    print("MESSAGES: " + msg.topic + " " + str(msg.qos) + " " + str(msg.payload))

def on_message_tare(client, userdata, msg):  # The callback for when a PUBLISH message 
    print("Auto_tare") 
    command = b'\x52\x0D' #tare comand
    ser.write(command)  

####################################################################################################
# Definiton of serial stuff
####################################################################################################

#configure the serial connections (the parameters differs on the device you are connecting to)
ser = serial.Serial(
    port='/dev/ttyUSB0',
    baudrate=19200,
    parity=serial.PARITY_ODD,
    stopbits=serial.STOPBITS_TWO,
    bytesize=serial.SEVENBITS
)

ser.isOpen()

#gws=netifaces.gateways()                                  #public gateway dns
#broker_ip=gws['default'].values()[0][0]
broker_ip="0.0.0.0" #local host of raspberry
port=1883

####################################################################################################
# Definiton of mqtt client stuff
####################################################################################################

client= mqtt.Client()                          #create client object with random id

client.message_callback_add("/dev1/trickler/esp/scale_request_tare", on_message_tare)
client.on_connect = on_connect
client.on_message = on_message

client.connect(broker_ip,port) 
client.loop_start()

starttime=time.time()
AliveCounter = 1

#Display Broker IP and sleep for 5 seconds
#print ("Broker IP:" + broker_ip)
#time.sleep(5)

####################################################################################################
# Handle loop
####################################################################################################

while True :
    
    #clear current weight string
    weight = ''
    
    #receive serial data
    while ser.inWaiting() > 0:
        weight += ser.read(1)
    
    #send weight to mqtt if weight is received
    if weight != '':    
         #print weight
         f=open("logfile.log","a")
         f.write(weight + "\n")
         f.close()
         client.publish(topic="/dev0/scale/current_weight", payload=weight,qos=0, retain=False)
    
    #send each second alive 
    if (time.time() - starttime) > 1:
       client.publish(topic="/dev0/scale/alive", payload=AliveCounter,qos=0, retain=False) 
       AliveCounter = AliveCounter + 1
       starttime=time.time()
    





