#include "Arduino.h"
#include "uMQTTBroker.h"


/*
 * Custom broker class with overwritten callback functions
 */
class myMQTTBroker: public uMQTTBroker
{
public:
    virtual bool onConnect(IPAddress addr, uint16_t client_count) {
      _new_data = addr.toString()+" connected" ;
      return true;
    }

    virtual void onData(String topic, const char *data, uint32_t length) {
      char data_str[length+1];
      os_memcpy(data_str, data, length);
      data_str[length] = '\0';

      String newData(data_str);
      _new_data = "received topic '"+topic+"' with data '"+ newData +"'";
    }

    inline String get_new_data()
    {
      return _new_data;
    }


private:
  String _new_data;
};
