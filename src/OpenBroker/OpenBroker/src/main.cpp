#include "myWiFi.hpp"
#include "myBroker.hpp"

String strMqttData = "";

myMQTTBroker myBroker;

void setup()
{
  Serial.begin(115200);
  Serial.println();

  // Start WiFi
  wifi_init(MODE::SOFT_AP);

  // Start the broker
  Serial.println("Starting MQTT broker");
  myBroker.init();
  myBroker.subscribe("#");
}



void loop()
{

  if(strMqttData != myBroker.get_new_data())
  {
    myBroker.publish("/broker/data",myBroker.get_new_data());
    strMqttData = myBroker.get_new_data();
  }

  //myBroker.cleanupClientConnections()
}
