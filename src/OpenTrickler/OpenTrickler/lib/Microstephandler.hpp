#ifndef MICROSTEPPHANDLER_HPP
#define MICROSTEPPHANDLER_HPP


#include "Arduino.h"

class Microstephandler
{
public:

  enum DRIVER
  {
    A4988 = 0,
    DRV8825 = 1
  };

  enum MICROSTEPMODE
  {
    FULL = 0,
    HALF = 1,
    QUARTER = 2,
    EIGTH = 3,
    SIXTEENTH = 4,
    THIRTYSECOND = 5
  };

  Microstephandler(const unsigned int Interface, const unsigned int pinM1, const unsigned int pinM2, const unsigned int pinM3):
  _Interface(Interface),_pinM1(pinM1),_pinM2(pinM2),_pinM3(pinM3),_ActiveMode(FULL),_pinStateM1(false), _pinStateM2(false),
  _pinStateM3(false)
  {
    pinMode(_pinM1,OUTPUT);
    pinMode(_pinM2,OUTPUT);
    pinMode(_pinM3,OUTPUT);
  }

  ~Microstephandler(){}

  inline void disableMicroStepping()
  {
    this->set(FULL);
  }

  inline void set(unsigned int Mode)
  {
    switch (_Interface)
    {
      case A4988:
      {
        if(Mode > SIXTEENTH)
        {
          Mode = SIXTEENTH;
        }

        _ActiveMode = Mode;

        setIOMappingA4988();

        break;
      }

      case DRV8825:
      {
        if(Mode > THIRTYSECOND)
        {
          Mode = THIRTYSECOND;
        }

        _ActiveMode = Mode;

        setIOMappingDRV8825();

        break;
      }
      default:
      {
        disableMicroStepping();
        break;
      }
    }

  }

private:

  const unsigned int _Interface;
  const unsigned int _pinM1;
  const unsigned int _pinM2;
  const unsigned int _pinM3;

  unsigned int _ActiveMode;

  bool _pinStateM1;
  bool _pinStateM2;
  bool _pinStateM3;

  inline void setIOMappingA4988()
  {
    switch (_ActiveMode)
   {
      case FULL:
      {
        _pinStateM1 = false;
        _pinStateM2 = false;
        _pinStateM3 = false;
        break;
      }

      case HALF:
      {
        _pinStateM1 = true;
        _pinStateM2 = false;
        _pinStateM3 = false;
        break;
      }

      case QUARTER:
      {
        _pinStateM1 = false;
        _pinStateM2 = true;
        _pinStateM3 = false;
        break;
      }

      case EIGTH:
      {
        _pinStateM1 = true;
        _pinStateM2 = true;
        _pinStateM3 = false;
        break;
      }

      case SIXTEENTH:
      {
        _pinStateM1 = true;
        _pinStateM2 = true;
        _pinStateM3 = true;
        break;
      }

      default:
      {
        this->disableMicroStepping();
        break;
      }
    }

    digitalWrite(_pinM1, _pinStateM1);
    digitalWrite(_pinM2, _pinStateM2);
    digitalWrite(_pinM3, _pinStateM3);
  }

  inline void setIOMappingDRV8825()
  {
    switch (_ActiveMode)
   {
      case FULL:
      {
        _pinStateM1 = false;
        _pinStateM2 = false;
        _pinStateM3 = false;
        break;
      }

      case HALF:
      {
        _pinStateM1 = true;
        _pinStateM2 = false;
        _pinStateM3 = false;
        break;
      }

      case QUARTER:
      {
        _pinStateM1 = false;
        _pinStateM2 = true;
        _pinStateM3 = false;
        break;
      }

      case EIGTH:
      {
        _pinStateM1 = true;
        _pinStateM2 = true;
        _pinStateM3 = false;
        break;
      }

      case SIXTEENTH:
      {
        _pinStateM1 = false;
        _pinStateM2 = false;
        _pinStateM3 = true;
        break;
      }

      case THIRTYSECOND:
      {
        _pinStateM1 = true;
        _pinStateM2 = true;
        _pinStateM3 = true;
        break;
      }

      default:
      {
        this->disableMicroStepping();
        break;
      }
    }

    digitalWrite(_pinM1, _pinStateM1);
    digitalWrite(_pinM2, _pinStateM2);
    digitalWrite(_pinM3, _pinStateM3);
  }

};



#endif /* end of include guard: MICROSTEPPHANDLER_HPP */
