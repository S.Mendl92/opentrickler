#include <Arduino.h>
#include "myHmi.hpp"
#include "myWiFi.hpp"
#include "myMqttClient.hpp"
#include "myStateMachine.hpp"
#include "myOpenScaleInterface.hpp"
#include "myAccelStepper.hpp"
#include "myFunctionals.hpp"
#include "myOta.hpp"

void setup() {

  Serial.begin(115200);
  Serial.println();

  // Start WiFi
  wifi_init(MODE::CLIENT);
  mqtt_init();

  //init ota
  ota_init();

  //init hmi
  hmi_init();

  //init steppers
  Stepper_init();

  //init scale
  mqttSendRequestTare();
}

void loop() {

  //////////////////////////////////////////////////////////////////////////////
  //Handle State independ stuff
  //////////////////////////////////////////////////////////////////////////////

  check_wifi_connection();
  check_mqtt_client_connection();

  check_mqtt_client_loop();

  handle_ota_loop();

  handle_open_scale_watchdog();

  //handle progres
  if (((millis() - ulSaveTimeStampProgress) >= 300) and (iCurrentStep > STATE::TARE_SCALE) and (iCurrentStep < STATE::REMOVE_WEIGHT))
  {
    mqttProgress((get_current_weight_mg()*100)/ RecVal.uiFillingWeight);

    //save time stamp
    ulSaveTimeStampProgress = millis();
  }

  //////////////////////////////////////////////////////////////////////////////
  //Handle Abort condition
  /////////////////////////////////////////////////////////////////////////////

  if(hmi.ActElm.btnAbort)
  {
    //set abort condition
    iNextStep = STATE::REMOVE_WEIGHT;

    //clear button flag
    hmi.ActElm.btnAbort = false;
  }

  //////////////////////////////////////////////////////////////////////////////
  //Handle Edges
  //////////////////////////////////////////////////////////////////////////////

  bPeChangeState = (iNextStep != iCurrentStep);

  //////////////////////////////////////////////////////////////////////////////
  //State Machine
  //////////////////////////////////////////////////////////////////////////////

  //copy next Step
  iCurrentStep = iNextStep;

  switch (iCurrentStep) {

    case STATE::IDLE:
    {
      //////////////////////////////////////////////////////////////////////////
      //Current State IDLE
      //////////////////////////////////////////////////////////////////////////

      bool bStartAutoMode = false;
      bool bStartSingleMode = false;
      bool bStartFineFillerSettingMode = false;

      if(bPeChangeState)
      {
        mqttDisplay("State: IDLE - State ID " +String(iCurrentStep));

        //clear requests
        hmi.ActElm.btnStartSingleAction = false;

        //clear variable
        bSaveInitWeight = false;
        bNewContainerAvailable = false;

        //save active opmode
        iSavedActiveOperationMode = RecVal.uiOperationMode;
      }

      check_new_hmi_parameter_download();

      //handle save init weight
      if(get_stable_weight_state() and not bSaveInitWeight)
      {
        iSavedInitWeight = get_current_weight_mg();
        bSaveInitWeight = true;
      }
      else if(bSaveInitWeight and (get_current_weight_mg() >= (iSavedInitWeight + ciContainerWeight)) and get_stable_weight_state() and not bNewContainerAvailable)
      {
        bNewContainerAvailable = true;
      }

      //Start automode if weight ist higher than ciStartWeight
      bStartAutoMode = ((RecVal.uiOperationMode == 0) and bNewContainerAvailable);

      //Start single mode or prefill setting mode if request is true
      bStartSingleMode = ((RecVal.uiOperationMode == 1) or (RecVal.uiOperationMode == 3)) and hmi.ActElm.btnStartSingleAction;

      //start fine filler setting mode
      bStartFineFillerSettingMode = (RecVal.uiOperationMode == 2);


      //////////////////////////////////////////////////////////////////////////
      //Handle Next State
      //////////////////////////////////////////////////////////////////////////

      if(bStartAutoMode or bStartSingleMode)
      {
        //clear request
        hmi.ActElm.btnStartSingleAction = false;

        //next step
        iNextStep = STATE::TARE_SCALE;
      }
      else if (bStartFineFillerSettingMode)
      {
        iNextStep = STATE::FINEFILLER_SETTING;
      }
      else if ((iSavedActiveOperationMode == 3) and (RecVal.uiOperationMode != 3))
      {
        iNextStep = STATE::REMOVE_WEIGHT;
      }
      else
      {
        iNextStep = iCurrentStep;
      }

      break;
    }

    case STATE::TARE_SCALE:
    {
      //////////////////////////////////////////////////////////////////////////
      //Current State TARE_SCALE
      //////////////////////////////////////////////////////////////////////////

      const unsigned long culStateChangeTime = 1000;

      if(bPeChangeState)
      {
        mqttSendRequestTare();
        mqttDisplay("State: Tare Scale - State ID " +String(iCurrentStep));
        bRequestChangeState = false;
      }

      //change state if value is in range of cuiLevel and values are stable
      if (func_in_range(5,get_current_weight_mg(),-5) and get_stable_weight_state());
      {
        //save once timestamp
        if(!bRequestChangeState)
        {
          ulSaveTimeStamp = millis();
        }

        //save request
        bRequestChangeState = true;
      }

      //////////////////////////////////////////////////////////////////////////
      //Handle Next State
      //////////////////////////////////////////////////////////////////////////

      if(bRequestChangeState and ((millis() - ulSaveTimeStamp) >= culStateChangeTime))
      {
        //next step
        iNextStep = STATE::HANDLE_PREFILLER;

        //create new plotter value dataset
        if(RecVal.uiOperationMode == 0 || RecVal.uiOperationMode == 1)
        {
          fillingID += 1;
          fillingTime = millis();
        }

      }
      else
      {
        iNextStep = iCurrentStep;
      }

      break;
    }

    case STATE::HANDLE_PREFILLER:
    {
      //////////////////////////////////////////////////////////////////////////
      //Current State FORWARD_PREFILLER
      //////////////////////////////////////////////////////////////////////////

      if(bPeChangeState)
      {
        mqttDisplay("State: Handle PreFiller - State ID " +String(iCurrentStep));

        FineFiller.setSpeed(RecVal.uiFineFiller_Velocity_full);
        FineFillerMicroStepHandler.set(Microstephandler::FULL);

        PreFiller.setSpeed(RecVal.uiPreFiller_Velocity_Fast);
        //digitalWrite(pinPreFillerDirection,false);
      }

      //calculate filling weight delta
      const int FillingWeightDelta = RecVal.uiFillingWeight - get_current_weight_mg();

      //change speed of prefiller
      if (FillingWeightDelta <= RecVal.uiPreFiller_Weight_Slow)
      {
        PreFiller.setSpeed(RecVal.uiPreFiller_Velocity_Slow);
      }

      //Handle PreFiller
      PreFiller.runSpeed();

      //Handle FineFiller
      if((RecVal.uiOperationMode != 3) and (RecVal.uiPreFiller_EnableFineFiller == 1))
      {
        FineFiller.runSpeed();
      }

      bool bPreFillingProcessFinished = (FillingWeightDelta <= RecVal.uiPreFiller_Weight_Stop);

      //////////////////////////////////////////////////////////////////////////
      //Handle Next State
      //////////////////////////////////////////////////////////////////////////

      if(bPreFillingProcessFinished and (RecVal.uiOperationMode == 3))
      {
        iNextStep = STATE::IDLE;

        PreFiller.disableOutputs();
      }

      else if(bPreFillingProcessFinished and (RecVal.uiOperationMode != 3))
      {
        iNextStep = STATE::HANDLE_FINEFILLER;

        PreFiller.disableOutputs();
      }
      else
      {
        iNextStep = iCurrentStep;
      }

      break;
    }

    case STATE::HANDLE_FINEFILLER:
    {
      //////////////////////////////////////////////////////////////////////////
      //Current State HANDLE_FINEFILLER
      //////////////////////////////////////////////////////////////////////////

      if(bPeChangeState)
      {
        mqttDisplay("State: Move FineFiller - State ID " +String(iCurrentStep));
        FineFiller.setSpeed(RecVal.uiFineFiller_Velocity_full);
        ulSaveTimeStamp = millis();
      }

      const int FillingWeightDelta = RecVal.uiFillingWeight - get_current_weight_mg();

      if((FillingWeightDelta <= RecVal.uiFineFiller_TrickleWeight_sixteenth) or
         (FillingWeightDelta <= 0))
      {
        FineFillerMicroStepHandler.set(Microstephandler::SIXTEENTH);
        FineFiller.setSpeed(RecVal.uiFineFiller_Velocity_sixteenth);
      }
      else if ((FillingWeightDelta > RecVal.uiFineFiller_TrickleWeight_sixteenth) and
              (FillingWeightDelta <= RecVal.uiFineFiller_TrickleWeight_eigth))
      {
        FineFillerMicroStepHandler.set(Microstephandler::EIGTH);
        FineFiller.setSpeed(RecVal.uiFineFiller_Velocity_eigth);
      }
      else if ((FillingWeightDelta > RecVal.uiFineFiller_TrickleWeight_eigth) and
              (FillingWeightDelta <= RecVal.uiFineFiller_TrickleWeight_quarter))
      {
        FineFillerMicroStepHandler.set(Microstephandler::QUARTER);
        FineFiller.setSpeed(RecVal.uiFineFiller_Velocity_quarter);
      }
      else if ((FillingWeightDelta > RecVal.uiFineFiller_TrickleWeight_quarter) and
              (FillingWeightDelta <= RecVal.uiFineFiller_TrickleWeight_half))
      {
        FineFillerMicroStepHandler.set(Microstephandler::HALF);
        FineFiller.setSpeed(RecVal.uiFineFiller_Velocity_half);
      }
      else
      {
        FineFillerMicroStepHandler.set(Microstephandler::FULL);
        FineFiller.setSpeed(RecVal.uiFineFiller_Velocity_full);
      }

      bool bFillingProcessFinished = (FillingWeightDelta <= RecVal.iFineFiller_Bias);

      FineFiller.runSpeed();

      //Check if filling time is exceeded, disable if time monitoring 0
      if((millis()-ulSaveTimeStamp) >= RecVal.uiFineFiller_MaxFineFillingTime and RecVal.uiFineFiller_MaxFineFillingTime != 0)
      {
        mqttLogger("Filling Time Exceeded");
        bFillingProcessFinished = true;
      }

      //////////////////////////////////////////////////////////////////////////
      //Handle Next State
      //////////////////////////////////////////////////////////////////////////

      if(bFillingProcessFinished)
      {
        iNextStep = STATE::REMOVE_WEIGHT;

        //disable outputs to avoid an overheating of the stepper
        FineFiller.disableOutputs();
        FineFillerMicroStepHandler.disableMicroStepping();

        //Send progress finished
        mqttProgress(100);
      }
      else
      {
        iNextStep = iCurrentStep;
      }

      break;
    }

    case STATE::REMOVE_WEIGHT:
    {
      //////////////////////////////////////////////////////////////////////////
      //Current State REMOVE_WEIGHT
      //////////////////////////////////////////////////////////////////////////

      if(bPeChangeState)
      {
        mqttDisplay("State: Remove Weight - State ID " +String(iCurrentStep));
        bSendTareRequest = false;

        iSavedRemoveWeight = get_current_weight_mg();

        //send plotter values
        if(RecVal.uiOperationMode == 0 || RecVal.uiOperationMode == 1)
        {
          mqttPlotter(fillingID,millis()-fillingTime,get_current_weight_mg());
        }
      }

      //create Request Change state
      bool bRequestChangeState = (get_current_weight_mg() <= (iSavedRemoveWeight-ciContainerWeight));

      //////////////////////////////////////////////////////////////////////////
      //Handle Next State
      //////////////////////////////////////////////////////////////////////////

      if(bRequestChangeState)
      {
        //next step
        iNextStep = STATE::IDLE;

        //clear progres
        mqttProgress(0);
      }
      else
      {
        iNextStep = iCurrentStep;
      }

      break;
    }

    case STATE::FINEFILLER_SETTING:
    {
      //////////////////////////////////////////////////////////////////////////
      //Current State FINEFILLER_SETTING
      //////////////////////////////////////////////////////////////////////////

      if(bPeChangeState)
      {
        mqttDisplay("State: FineFiller - Setting - State ID " +String(iCurrentStep));
        FineFiller.setSpeed(RecVal.uiFineFiller_Velocity_full);
        FineFillerMicroStepHandler.set(Microstephandler::FULL);

        //clear fine filler setting opmode
        mqttSendRequestDisableFineFillerSettingMode();
      }

      if(check_new_hmi_parameter_download())
      {
        unsigned int Mode = 0;

        //Mapping defined in mqtt dashboard
        switch (RecVal.uiFineFillerSettingMode)
        {
          case 1:
            {
              Mode = Microstephandler::FULL;
              FineFiller.setSpeed(RecVal.uiFineFiller_Velocity_full);
              break;
            }
          case 2:
            {
              Mode = Microstephandler::HALF;
              FineFiller.setSpeed(RecVal.uiFineFiller_Velocity_half);
              break;
            }
          case 3:
            {
              Mode = Microstephandler::QUARTER;
              FineFiller.setSpeed(RecVal.uiFineFiller_Velocity_quarter);
              break;
            }
          case 4:
            {
              Mode = Microstephandler::EIGTH;
              FineFiller.setSpeed(RecVal.uiFineFiller_Velocity_eigth);
              break;
            }
          case 5:
            {
              Mode = Microstephandler::SIXTEENTH;
              FineFiller.setSpeed(RecVal.uiFineFiller_Velocity_sixteenth);
              break;
            }
          default:
          {
            Mode = Microstephandler::FULL;
            FineFiller.setSpeed(RecVal.uiFineFiller_Velocity_full);
            break;
          }
        }

        FineFillerMicroStepHandler.set(Mode);
      }

      if(RecVal.uiFineFillerSettingMode > 0)
      {
        FineFiller.runSpeed();
      }

      //////////////////////////////////////////////////////////////////////////
      //Handle Next State
      //////////////////////////////////////////////////////////////////////////

      //use hmi parameter instead of esp parameter
      if(hmi.RecVal.uiOperationMode != 2)
      {
        //next step
        iNextStep = STATE::REMOVE_WEIGHT;

        //disable outputs
        FineFiller.disableOutputs();

        //clear progres
        mqttProgress(0);

        //clear fine filler setting opmode
        mqttSendRequestDisableFineFillerSettingMode();
      }
      else
      {
        iNextStep = iCurrentStep;
      }

      break;
    }
  }


}
