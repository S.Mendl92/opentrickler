#include <Arduino.h>


unsigned long ulOldOpenScaleWatchdogCounter = 0;
unsigned long ulCurrentOpenScaleWatchdogCounter = 0;

unsigned long ulOpenScaleMessageCounter = 0;

unsigned long ulOpenScaleCurrentCycleTime = 0;

int iCurrentWeight = 0;
bool bStableWeight = false;

bool bOpenScaleConnected = false;

void handle_OpenScale_callback(const String topic, const String message)
{
  if (topic == "/dev0/scale/alive")
  {
    ulCurrentOpenScaleWatchdogCounter += 1;
  }

  if (topic == "/dev0/scale/current_weight")
  {
    int SlicedWeight[7] = {0};

    SlicedWeight[6] = String(message[4]).toInt()*1000000;
    SlicedWeight[5] = String(message[5]).toInt()*100000;
    SlicedWeight[4] = String(message[6]).toInt()*10000;
    SlicedWeight[3] = String(message[7]).toInt()*1000;
    //message [8] = .
    SlicedWeight[2] = String(message[9]).toInt()*100;
    SlicedWeight[1] = String(message[10]).toInt()*10;
    SlicedWeight[0] = String(message[11]).toInt()*1;

    //clear current weight
    iCurrentWeight = 0;

    //calculate current weight
    for(int i = 0; i<=6; i+=1)
    {
      iCurrentWeight += SlicedWeight[i];
    }

    //invert current weight
    if (message[3] == '-')
    {
      iCurrentWeight *= -1;
    }

    //Check if weight is stable
    bStableWeight =  (message[0] == 'S' and message[1] == 'T');

    //increase message counter
    ulOpenScaleMessageCounter += 1;
  }
}

//declared in myMqttClient.hpp
void mqttOpenScaleConnection(const bool bConnected);

bool handle_open_scale_watchdog()
{
    const unsigned long culMessurmentTime = 5000;

    if ((millis() - ulOpenScaleCurrentCycleTime) > culMessurmentTime)
    {
      //save current cycle time
      ulOpenScaleCurrentCycleTime = millis();

      //check connection state
      bOpenScaleConnected = (ulOldOpenScaleWatchdogCounter != ulCurrentOpenScaleWatchdogCounter);

      //copy new connection value to old connection value
      ulOldOpenScaleWatchdogCounter = ulCurrentOpenScaleWatchdogCounter;

      //send new conenction message
      mqttOpenScaleConnection(bOpenScaleConnected);
    }

    return bOpenScaleConnected;
}

inline int get_current_weight_mg()
{
  return iCurrentWeight;
}

inline unsigned long get_scale_message_counter()
{
  return ulOpenScaleMessageCounter;
}

inline bool get_stable_weight_state()
{
  return bStableWeight;
}

//definded in myMQTTClient
void mqttSendRequestTare();
