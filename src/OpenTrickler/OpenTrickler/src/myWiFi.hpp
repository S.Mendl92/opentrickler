//V0.1 1.05.2020

#include <ESP8266WiFi.h>

//////////////////////////////////////////////////////////////////////
//Set global Parameter

const String ssid = "FRITZ!Box 7490";     // your network SSID (name)
const String pass = "57395180922115593323";         // your network password


//////////////////////////////////////////////////////////////////////

//Enum: includes init modes for Wifi
enum MODE{
  SOFT_AP = 0,
  CLIENT = 1
};

//init wifi module
//param uiMode: set int mode 0 = Soft AP, 1= Client
//return: void
void wifi_init(const unsigned int uiMode)
{

  switch (uiMode) {

    case MODE::SOFT_AP:
    {

      //////////////////////////////////////////////////////////////
      // Init Wifi as Soft AP
      //////////////////////////////////////////////////////////////

      WiFi.mode(WIFI_AP);
      WiFi.softAP(ssid, pass);
      Serial.println("AP started");
      Serial.println("IP address: " + WiFi.softAPIP().toString());

      break;
    }

    case MODE::CLIENT:
    {

      //////////////////////////////////////////////////////////////
      // Init Wifi as Client
      //////////////////////////////////////////////////////////////

      Serial.println("Connecting to "+(String)ssid);
      WiFi.begin(ssid, pass);

      unsigned int iNoConnectionCounter = 0;  //counts number of connection try's

      //check connection state
      while (WiFi.status() != WL_CONNECTED) {
        delay(500);
        Serial.print(".");

        iNoConnectionCounter += 1;

        //restart if no connection
        if (iNoConnectionCounter == 30)
        {
          ESP.restart();
          Serial.println("Restart of device");
        }
      }
      Serial.println("");

      Serial.println("WiFi connected");
      Serial.println("IP address: " + WiFi.localIP().toString());

      break;
    }

    default:
    {

      //////////////////////////////////////////////////////////////
      // Wrong Init Mode
      //////////////////////////////////////////////////////////////

      Serial.println("Restart of device");
      ESP.restart();

      break;
    }
  }
}

//check wifi connection
//param u
//return: void
void check_wifi_connection()
{
  unsigned int iNoConnectionCounter = 0;  //counts number of connection try's

  //check connection state
  while (WiFi.status() != WL_CONNECTED) {

    Serial.println("Try to reconnect to WiFi " + ssid);
    delay(500);

    iNoConnectionCounter += 1;

    //restart if no connection
    if (iNoConnectionCounter == 30)
    {
      Serial.println("Restart of device");
      ESP.restart();
    }
  }
}
