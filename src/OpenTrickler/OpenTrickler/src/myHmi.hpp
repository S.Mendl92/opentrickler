#include <Arduino.h>
#include <EEPROM.h>


const unsigned int cuiSizeOfEeprom = 2096;

// Types 'byte' and 'word' doesn't work!
struct stRecipeParameter
{
  unsigned int uiOperationMode;
  unsigned int uiFineFillerSettingMode;
  unsigned int uiFillingWeight;

  unsigned int uiPreFiller_Velocity_Fast;
  unsigned int uiPreFiller_Weight_Slow;
  unsigned int uiPreFiller_Velocity_Slow;
  unsigned int uiPreFiller_Weight_Stop;
  unsigned int uiPreFiller_EnableFineFiller;

  unsigned int uiFineFiller_Velocity_full;
  unsigned int uiFineFiller_Velocity_half;
  unsigned int uiFineFiller_Velocity_quarter;
  unsigned int uiFineFiller_Velocity_eigth;
  unsigned int uiFineFiller_Velocity_sixteenth;

  unsigned int uiFineFiller_TrickleWeight_half;
  unsigned int uiFineFiller_TrickleWeight_quarter;
  unsigned int uiFineFiller_TrickleWeight_eigth;
  unsigned int uiFineFiller_TrickleWeight_sixteenth;

  int          iFineFiller_Bias;

  unsigned int uiFineFiller_MaxFineFillingTime;
};

struct stActionElement
{
  bool btnStartSingleAction;
  bool btnSaveConfig;
  bool btnAbort;
};

struct stInterface
{
  stRecipeParameter RecVal;
  int iEepromStartAdress;
  stActionElement ActElm;
};

struct stUpload
{
  unsigned int IdHmi;
  unsigned int IdEsp;
};

stInterface hmi;
stRecipeParameter RecVal;
stUpload Upload;

////////////////////////////////////////////////////////////////////////////////

inline void saveRecipeParameter() {
  // Save configuration from RAM into EEPROM
  Serial.println("Save configuration from RAM into EEPROM");
  EEPROM.begin(cuiSizeOfEeprom);
  EEPROM.put(hmi.iEepromStartAdress, hmi.RecVal);
  delay(100);
  EEPROM.commit();                      // Only needed for ESP8266 to get data written
  EEPROM.end();                         // Free RAM copy of structure
}

inline void loadRecipeParameter() {
  // Loads configuration from EEPROM into RAM
  Serial.println("Loads configuration from EEPROM into RAM");
  EEPROM.begin(cuiSizeOfEeprom);
  EEPROM.get(hmi.iEepromStartAdress, hmi.RecVal);
  EEPROM.end();
}

inline void hmi_init() {
  //init action elements
  hmi.ActElm.btnStartSingleAction = false;
  hmi.ActElm.btnSaveConfig = false;
  hmi.ActElm.btnAbort = false;

  //init upload id
  Upload.IdHmi = 0;
  Upload.IdEsp = 0;

  //load last saved recipe parameter
  loadRecipeParameter();

  //copy last saved recipe parameters to internal struct
  RecVal = hmi.RecVal;
}

//declared in myMqttClient.hpp
void mqttDisplayHmiSaveConfig();
void mqttSendActivePrameter(const String topic, const String msg);

inline bool check_new_hmi_parameter_download()
{

  bool RetVal = false;

  if(Upload.IdEsp != Upload.IdHmi)
  {
    //copy last saved recipe parameters to internal struct
    RecVal = hmi.RecVal;

    //save upload id
    Upload.IdEsp = Upload.IdHmi;

    const unsigned int delaytime = 10;

    //upload new parameter to user interface
    mqttSendActivePrameter("/dev1/trickler/active_prameter/filling_weight", String(RecVal.uiFillingWeight));delay(delaytime);
    mqttSendActivePrameter("/dev1/trickler/active_prameter/prefiller_velocity_fast", String(RecVal.uiPreFiller_Velocity_Fast));delay(delaytime);
    mqttSendActivePrameter("/dev1/trickler/active_prameter/prefiller_weight_slow", String(RecVal.uiPreFiller_Weight_Slow));delay(delaytime);
    mqttSendActivePrameter("/dev1/trickler/active_prameter/prefiller_velocity_slow", String(RecVal.uiPreFiller_Velocity_Slow));delay(delaytime);
    mqttSendActivePrameter("/dev1/trickler/active_prameter/prefiller_weight_stop", String(RecVal.uiPreFiller_Weight_Stop));delay(delaytime);
    mqttSendActivePrameter("/dev1/trickler/active_prameter/prefiller_handle_finefiller", String(RecVal.uiPreFiller_EnableFineFiller));delay(delaytime);
    mqttSendActivePrameter("/dev1/trickler/active_prameter/finefiller_velocity_1_1", String(RecVal.uiFineFiller_Velocity_full));delay(delaytime);
    mqttSendActivePrameter("/dev1/trickler/active_prameter/finefiller_velocity_1_2", String(RecVal.uiFineFiller_Velocity_half));delay(delaytime);
    mqttSendActivePrameter("/dev1/trickler/active_prameter/finefiller_velocity_1_4", String(RecVal.uiFineFiller_Velocity_quarter));delay(delaytime);
    mqttSendActivePrameter("/dev1/trickler/active_prameter/finefiller_velocity_1_8", String(RecVal.uiFineFiller_Velocity_eigth));delay(delaytime);
    mqttSendActivePrameter("/dev1/trickler/active_prameter/finefiller_velocity_1_16", String(RecVal.uiFineFiller_Velocity_sixteenth));delay(delaytime);
    mqttSendActivePrameter("/dev1/trickler/active_prameter/finefiller_trickle_weight_1_2", String(RecVal.uiFineFiller_TrickleWeight_half));delay(delaytime);
    mqttSendActivePrameter("/dev1/trickler/active_prameter/finefiller_trickle_weight_1_4", String(RecVal.uiFineFiller_TrickleWeight_quarter));delay(delaytime);
    mqttSendActivePrameter("/dev1/trickler/active_prameter/finefiller_trickle_weight_1_8", String(RecVal.uiFineFiller_TrickleWeight_eigth));delay(delaytime);
    mqttSendActivePrameter("/dev1/trickler/active_prameter/finefiller_trickle_weight_1_16", String(RecVal.uiFineFiller_TrickleWeight_sixteenth));delay(delaytime);
    mqttSendActivePrameter("/dev1/trickler/active_prameter/finefiller_bias", String(RecVal.iFineFiller_Bias));delay(delaytime);
    mqttSendActivePrameter("/dev1/trickler/active_prameter/finefiller_max_filling_time", String(RecVal.uiFineFiller_MaxFineFillingTime));delay(delaytime);
    //mqttSendActivePrameter("", String(RecVal));delay(delaytime);

    RetVal = true;
  }

  //Save hmi parameter if request
  if (hmi.ActElm.btnSaveConfig)
  {
    saveRecipeParameter();
    mqttDisplayHmiSaveConfig();
    hmi.ActElm.btnSaveConfig = false;
  }

  return RetVal;

}
//declared in myMqttClient.hpp
void mqttDisplay(const String msg);
void mqttDisplayHmiUploadId(const unsigned int uploadId);
void mqttLogger(const String msg);

void handle_hmi_recipeparameter_callback(const String topic, const String message)
{

  if (topic == "/dev1/trickler/recipe/set_mode")
  {
    hmi.RecVal.uiOperationMode = (unsigned int)message.toInt();       //Convert message to value
    Upload.IdHmi += 1;                                                //increase upload id

    mqttDisplayHmiUploadId(Upload.IdHmi);
    mqttLogger("OpMode: " + String(hmi.RecVal.uiOperationMode));
  }

  if (topic == "/dev1/trickler/recipe/FineFillerSettingMode")
  {
    hmi.RecVal.uiFineFillerSettingMode = (unsigned int)message.toInt();       //Convert message to value
    Upload.IdHmi += 1;                                                //increase upload id

    mqttDisplayHmiUploadId(Upload.IdHmi);
    mqttLogger("OpMode Fine Filler - Setting Mode: " + String(hmi.RecVal.uiFineFillerSettingMode));
  }

  if (topic == "/dev1/trickler/recipe/filling_weight")
  {
    hmi.RecVal.uiFillingWeight = (unsigned int)message.toInt();       //Convert message to value
    Upload.IdHmi += 1;                                  //increase upload id

    mqttDisplayHmiUploadId(Upload.IdHmi);
    mqttLogger("Filling Weihgt: " + String(hmi.RecVal.uiFillingWeight) + " mg");
  }

  if (topic == "/dev1/trickler/recipe/prefiller_velocity_fast")
  {
    hmi.RecVal.uiPreFiller_Velocity_Fast = (unsigned int)message.toInt();     //Convert message to value
    Upload.IdHmi += 1;                                                   //increase upload id

    mqttDisplayHmiUploadId(Upload.IdHmi);
    mqttLogger("PF Velocity Fast: " + String(hmi.RecVal.uiPreFiller_Velocity_Fast) + " Steps/s");
  }

  if (topic == "/dev1/trickler/recipe/prefiller_weight_slow")
  {
    hmi.RecVal.uiPreFiller_Weight_Slow = (unsigned int)message.toInt();     //Convert message to value
    Upload.IdHmi += 1;                                                   //increase upload id

    mqttDisplayHmiUploadId(Upload.IdHmi);
    mqttLogger("PF Weight Slow: " + String(hmi.RecVal.uiPreFiller_Weight_Slow) + " mg");
  }

  if (topic == "/dev1/trickler/recipe/prefiller_velocity_slow")
  {
    hmi.RecVal.uiPreFiller_Velocity_Slow = (unsigned int)message.toInt();     //Convert message to value
    Upload.IdHmi += 1;                                                   //increase upload id

    mqttDisplayHmiUploadId(Upload.IdHmi);
    mqttLogger("PF Velocity Slow: " + String(hmi.RecVal.uiPreFiller_Velocity_Slow) + " Steps/s");
  }

  if (topic == "/dev1/trickler/recipe/prefiller_weight_stop")
  {
    hmi.RecVal.uiPreFiller_Weight_Stop = (unsigned int)message.toInt();     //Convert message to value
    Upload.IdHmi += 1;                                                   //increase upload id

    mqttDisplayHmiUploadId(Upload.IdHmi);
    mqttLogger("PF Weight Stop: " + String(hmi.RecVal.uiPreFiller_Weight_Slow) + " mg");
  }

  if (topic == "/dev1/trickler/recipe/prefiller_finefillermode")
  {
    hmi.RecVal.uiPreFiller_EnableFineFiller = (unsigned int)message.toInt();   //Convert message to value
    Upload.IdHmi += 1;                                                    //increase upload id

    mqttDisplayHmiUploadId(Upload.IdHmi);
    mqttLogger("PF Handle FineFiller: " + String(hmi.RecVal.uiPreFiller_EnableFineFiller));
  }

  if (topic == "/dev1/trickler/recipe/finefiller_velocity_full")
  {
    hmi.RecVal.uiFineFiller_Velocity_full = (unsigned int)message.toInt();     //Convert message to value
    Upload.IdHmi += 1;                                                    //increase upload id

    mqttDisplayHmiUploadId(Upload.IdHmi);
    mqttLogger("FF Velocity {full}: " + String(hmi.RecVal.uiFineFiller_Velocity_full) + " Steps/s");
  }

  if (topic == "/dev1/trickler/recipe/finefiller_velocity_half")
  {
    hmi.RecVal.uiFineFiller_Velocity_half = (unsigned int)message.toInt();     //Convert message to value
    Upload.IdHmi += 1;                                                    //increase upload id

    mqttDisplayHmiUploadId(Upload.IdHmi);
    mqttLogger("FF Velocity {half}: " + String(hmi.RecVal.uiFineFiller_Velocity_half) + " Steps/s");
  }

  if (topic == "/dev1/trickler/recipe/finefiller_velocity_quarter")
  {
    hmi.RecVal.uiFineFiller_Velocity_quarter = (unsigned int)message.toInt();     //Convert message to value
    Upload.IdHmi += 1;                                                    //increase upload id

    mqttDisplayHmiUploadId(Upload.IdHmi);
    mqttLogger("FF Velocity {quarter}: " + String(hmi.RecVal.uiFineFiller_Velocity_quarter) + " Steps/s");
  }

  if (topic == "/dev1/trickler/recipe/finefiller_velocity_eigth")
  {
    hmi.RecVal.uiFineFiller_Velocity_eigth = (unsigned int)message.toInt();     //Convert message to value
    Upload.IdHmi += 1;                                                    //increase upload id

    mqttDisplayHmiUploadId(Upload.IdHmi);
    mqttLogger("FF Velocity {eigth}: " + String(hmi.RecVal.uiFineFiller_Velocity_eigth) + " Steps/s");
  }

  if (topic == "/dev1/trickler/recipe/finefiller_velocity_sixteenth")
  {
    hmi.RecVal.uiFineFiller_Velocity_sixteenth = (unsigned int)message.toInt();     //Convert message to value
    Upload.IdHmi += 1;                                                    //increase upload id

    mqttDisplayHmiUploadId(Upload.IdHmi);
    mqttLogger("FF Velocity {sixteenth}: " + String(hmi.RecVal.uiFineFiller_Velocity_sixteenth) + " Steps/s");
  }

  if (topic == "/dev1/trickler/recipe/finefiller_trickleweight_half")
  {
    hmi.RecVal.uiFineFiller_TrickleWeight_half = (unsigned int)message.toInt();     //Convert message to value
    Upload.IdHmi += 1;                                                    //increase upload id

    mqttDisplayHmiUploadId(Upload.IdHmi);
    mqttLogger("FF TW {half}: " + String(hmi.RecVal.uiFineFiller_TrickleWeight_half) + " Delta mg");
  }

  if (topic == "/dev1/trickler/recipe/finefiller_trickleweight_quarter")
  {
    hmi.RecVal.uiFineFiller_TrickleWeight_quarter = (unsigned int)message.toInt();     //Convert message to value
    Upload.IdHmi += 1;                                                    //increase upload id

    mqttDisplayHmiUploadId(Upload.IdHmi);
    mqttLogger("FF TW {quarter}: " + String(hmi.RecVal.uiFineFiller_TrickleWeight_quarter) + " Delta mg");
  }

  if (topic == "/dev1/trickler/recipe/finefiller_trickleweight_eigth")
  {
    hmi.RecVal.uiFineFiller_TrickleWeight_eigth = (unsigned int)message.toInt();     //Convert message to value
    Upload.IdHmi += 1;                                                    //increase upload id

    mqttDisplayHmiUploadId(Upload.IdHmi);
    mqttLogger("FF TW {eigth}: " + String(hmi.RecVal.uiFineFiller_TrickleWeight_eigth) + " Delta mg");
  }

  if (topic == "/dev1/trickler/recipe/finefiller_trickleweight_sixteenth")
  {
    hmi.RecVal.uiFineFiller_TrickleWeight_sixteenth = (unsigned int)message.toInt();     //Convert message to value
    Upload.IdHmi += 1;                                                    //increase upload id

    mqttDisplayHmiUploadId(Upload.IdHmi);
    mqttLogger("FF TW {sixteenth}: " + String(hmi.RecVal.uiFineFiller_TrickleWeight_sixteenth) + " Delta mg");
  }

  if (topic == "/dev1/trickler/recipe/finefiller_bias")
  {
    hmi.RecVal.iFineFiller_Bias = (unsigned int)message.toInt();     //Convert message to value
    Upload.IdHmi += 1;                                                    //increase upload id

    mqttDisplayHmiUploadId(Upload.IdHmi);
    mqttLogger("FF Bias: " + String(hmi.RecVal.iFineFiller_Bias) + " mg");
  }

  if (topic == "/dev1/trickler/recipe/finefiller_max_filling_time")
  {
    hmi.RecVal.uiFineFiller_MaxFineFillingTime = (unsigned int)message.toInt();  //Convert message to value
    Upload.IdHmi += 1;                                                           //increase upload id

    mqttDisplayHmiUploadId(Upload.IdHmi);
    mqttLogger("FF Max Fine Filling Time: " + String(hmi.RecVal.uiFineFiller_MaxFineFillingTime) + " ms");
  }

}

void handle_hmi_button_callback(const String topic, const String message)
{
  if (topic == "/dev1/trickler/button/single_action")
  {
    hmi.ActElm.btnStartSingleAction = true;
    Serial.println("Request Start Single Action");
  }

  if (topic == "/dev1/trickler/button/save_config")
  {
    hmi.ActElm.btnSaveConfig = true;
    Serial.println("Request Save Config");
  }

  if (topic == "/dev1/trickler/button/abort")
  {
    hmi.ActElm.btnAbort = true;
    Serial.println("Request Button Abort");
  }

}
