#include <Arduino.h>
#include <ESP8266WiFi.h>
#include <PubSubClient.h>

WiFiClient espClient;
PubSubClient client(espClient);

bool bFoundBroker = false;

int ip[4] = {192,168,178,1};   //Fritz Box 
//int ip[4] = {192,168,2,100}; //Telekom Router

unsigned long ulWatchdogCurrentCycleTime = 0;
unsigned long ulWatchdogCounter = 0;

const String strTopicWatchdog = "/dev1/trickler/alive";

inline void mqttDisplay(const String msg)
{
  client.publish("/dev1/trickler/display",msg.c_str());
}

inline void mqttDisplayHmiUploadId(const unsigned int uploadId)
{
  client.publish("/dev1/trickler/uploadId",String(uploadId).c_str());
}

inline void mqttDisplayHmiSaveConfig()
{
  client.publish("/dev1/trickler/uploadId","succesfull");
}

inline void mqttLogger(const String msg)
{
  client.publish("/dev1/trickler/logger",msg.c_str());
}

inline void mqttPlotter(const unsigned int id, const long time, const int weight)
{
  const String msg = String(id) + ";" + String(time) + ";" + String(weight);
  client.publish("/dev1/trickler/esp/realtimeplotter",msg.c_str());
}

inline void mqttProgress(unsigned int  uiProgress)
{
  //fix progress if exceeded
  if (uiProgress > 100)
  {
    uiProgress = 100;
  }

  client.publish("/dev1/trickler/esp/progress",String(uiProgress).c_str());
}

inline void mqttOpenScaleConnection(const bool bConnected)
{
  if(bConnected)
  {
    client.publish("/dev1/trickler/esp/scale_connection_state","1");
  }
  else
  {
    client.publish("/dev1/trickler/esp/scale_connection_state","0");
  }
}

inline void mqttSendActivePrameter(const String topic, const String msg)
{
  client.publish(topic.c_str(),msg.c_str());
}

////////////////////////////////////////////////////////////////////////////////

inline void mqttSendRequestTare()
{
  client.publish("/dev1/trickler/esp/scale_request_tare","",2);
}

inline void mqttSendRequestDisableFineFillerSettingMode()
{
  client.publish("/dev1/trickler/recipe/FineFillerSettingMode","",2);
}

////////////////////////////////////////////////////////////////////////////////

void resubscribe(){
  client.subscribe("/dev1/#");
  client.subscribe("/dev0/scale/alive");
  client.subscribe("/dev0/scale/current_weight");
}

//declared in myHmi.hpp
void handle_hmi_recipeparameter_callback(const String topic, const String message);
void handle_hmi_button_callback(const String topic, const String message);
//declared in myOpenScaleInterface.hpp
void handle_OpenScale_callback(const String topic, const String message);

void callback(char* topic, byte* payload, unsigned int length) {
  Serial.print("Message arrived [");
  Serial.print(topic);
  Serial.print("] ");

  String strMessage = "";

  for (unsigned int i = 0; i < length; i++) {
    Serial.print((char)payload[i]);
    strMessage += String((char)payload[i]);
  }
  Serial.println();

  String strTopic(topic);

  handle_hmi_recipeparameter_callback(strTopic,strMessage);
  handle_hmi_button_callback(strTopic,strMessage);
  handle_OpenScale_callback(strTopic,strMessage);

}

void check_mqtt_client_connection() {

if (!client.connected()) {



  // Loop until we're reconnected
  while (!client.connected()) {
    Serial.print("Attempting MQTT connection...");
    // Create a random client ID
    String clientId = "ESP8266Client-";
    clientId += String(random(0xffff), HEX);

    if(ip[3] == 255)
    {
      ESP.restart();
    }

    // Attempt to connect
    if (client.connect(clientId.c_str())) {
      Serial.println("connected");
      // Once connected, publish an announcement...
      client.publish("outTopic", "hello world");
      // ... and resubscribe
      resubscribe();
    } else {
      Serial.print("failed, rc= ");
      Serial.print(client.state());
      Serial.print(" -> ");

      if (!bFoundBroker)
      {
        ip[3] += 1;
        client.setServer(IPAddress(ip[0],ip[1],ip[2],ip[3]), 1883);
        Serial.println("Test Broker IP: " + String(ip[0]) + "." + String(ip[1]) + "." + String(ip[2]) + "." + String(ip[3]));
      }

      //delay(250);

    }
  }
}
}

inline void check_mqtt_client_loop(){
  //handle mqtt loop
  client.loop();

  //publish watchdog each 1000ms
  if((millis() - ulWatchdogCurrentCycleTime) > 1000)
  {
    ulWatchdogCurrentCycleTime = millis();
    ulWatchdogCounter += 1;

    client.publish(strTopicWatchdog.c_str(),String(ulWatchdogCounter).c_str());
  }
}

inline void mqtt_init() {
  //client.setServer(WiFi.gatewayIP(), 1883);

  client.setServer(IPAddress(ip[0],ip[1],ip[2],ip[3]), 1883);
  //client.setServer("192.168.0.110", 1883);
  client.setCallback(callback);
}
