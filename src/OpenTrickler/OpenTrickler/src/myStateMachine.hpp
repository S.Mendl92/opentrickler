#include <Arduino.h>

enum STATE{
  IDLE = 0,
  TARE_SCALE = 500,
  HANDLE_PREFILLER = 1000,
  HANDLE_FINEFILLER = 2000,
  REMOVE_WEIGHT = 3000,
  FINEFILLER_SETTING = 10000
};

unsigned int iNextStep = STATE::IDLE;
unsigned int iCurrentStep = iNextStep + 1;    //this is required to init correct after boot

bool bPeChangeState = false;

unsigned long ulSaveTimeStamp = 0;

unsigned long ulSaveTimeStampProgress = millis();

const int ciContainerWeight = 2500;

//State IDLE
bool bSaveInitWeight = false;
bool bNewContainerAvailable = false;
int iSavedInitWeight = 0;
unsigned long iSavedActiveOperationMode = 0;

//State Tare scale
bool bRequestChangeState = false;

//State Remove Weight
bool bSendTareRequest = false;
int iSavedRemoveWeight = 0;

unsigned int fillingID = 0;
unsigned long fillingTime = 0;
