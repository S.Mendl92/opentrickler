#include <Arduino.h>

inline bool func_in_range(int up, int val, int low)
{
  return ((up >= val) and (low <= val));
}
