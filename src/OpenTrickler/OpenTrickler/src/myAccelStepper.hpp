#include <Arduino.h>
#include <AccelStepper.h>
#include "../lib/Microstephandler.hpp"

//this is required to fix direction bug of prefiller
const int pinPreFillerDirection = D7;

AccelStepper PreFiller(AccelStepper::DRIVER,D8,pinPreFillerDirection);//step, direction
AccelStepper FineFiller(AccelStepper::DRIVER,D3,D2); //step, direction
Microstephandler FineFillerMicroStepHandler(Microstephandler::A4988,D4,D5,D6); //M1,M2,M3

inline void Stepper_init()
{
  PreFiller.setAcceleration(3000.0);
  PreFiller.setMaxSpeed(1200);
  PreFiller.disableOutputs();

  FineFiller.setAcceleration(3000.0);
  FineFiller.setMaxSpeed(1200);

  FineFiller.disableOutputs();
  FineFillerMicroStepHandler.disableMicroStepping();
}
