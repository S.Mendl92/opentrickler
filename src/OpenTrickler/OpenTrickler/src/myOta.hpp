/*
  To upload through terminal you can use: curl -F "image=@firmware.bin" esp8266-webupdate.local/update
*/

#include <ESP8266WebServer.h>
#include <ESP8266HTTPUpdateServer.h>

ESP8266WebServer httpServer(80);
ESP8266HTTPUpdateServer httpUpdater;

inline void ota_init()
{
  httpUpdater.setup(&httpServer);
  httpServer.begin();
}

inline void handle_ota_loop()
{
  httpServer.handleClient();
}
