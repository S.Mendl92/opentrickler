# OpenTrickler

instal:

sudo apt-get install mosquitto
sudo apt-get install mosquitto-clients

#sudo apt-get install python3-tk
#pip3 install paho-mqtt
#pip3 install esptool
#pip3 install DateTime
#pip3 install pyserial

#pip install paho-mqtt
#pip3 install pyserial

ESP flashen:

    1. Vorbedingung:
        a.) Waage ist eingesteckt, ESP ist beim starten ausgesteckt
        b.) ESP einstecken

    2a. Möglichkeit 1 (mit Schwarzen Fenster - Terminal)
        a.) rechtklick in dem Ordner dieser Textdatei und dann "Open in Terminal -> Schwarzes Fenster öffnen"
        b.) folgendes eingeben:
            esptool.py --port /dev/ttyUSB1 --baud 115200 write_flash -fm dio -fs 32m 0x00000 firmware.bin
        c.) fertig!

    2b. Möglichkeit 2 (mit OpenTrickler Software)
        a.) opentrickler in schwarzen Fenster eingeben
        b.) load file klicken und die Datei firmware.bin in dem Ordner dieser Datei suchen
        c.) flash esp klicken -> dauert ein wenig
        d.) fertig
